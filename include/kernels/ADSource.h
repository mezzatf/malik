#pragma once

#include "ADKernel.h"

class ADSource : public ADKernel
{
public:
  static InputParameters validParams();

  ADSource(const InputParameters & parameters);

protected:
  virtual ADReal computeQpResidual() override;
  const ADVariableValue & _charge_density;

};
