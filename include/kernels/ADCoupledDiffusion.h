#pragma once

#include "ADKernelValue.h"

/**
 * Kernel which implements the convective term in the transient heat
 * conduction equation, and provides coupling with the Darcy voltage
 * equation.
 */
class ADCoupledDiffusion : public ADKernelValue
{
public:
  static InputParameters validParams();

  ADCoupledDiffusion(const InputParameters & parameters);

protected:
  /// ADKernelValue objects must override precomputeQpResidual
  virtual ADReal precomputeQpResidual() override;

  /// The gradient of voltage
  const ADVariableSecond & _voltage_gradSec;

  /// These references will be set by the initialization list so that
  /// values can be pulled from the Material system.
  const ADMaterialProperty<Real> & _mobility;

};
