#include "ADCoupledDiffusion.h"

registerMooseObject("malikApp", ADCoupledDiffusion);

InputParameters
ADCoupledDiffusion::validParams()
{
  InputParameters params = ADKernelValue::validParams();
  params.addRequiredCoupledVar("voltage", "The variable representing the voltage.");
  return params;
}

ADCoupledDiffusion::ADCoupledDiffusion(const InputParameters & parameters)
  : ADKernelValue(parameters),
    // Couple to the gradient of the voltage
    _voltage_gradSec(adCoupledSecond("voltage")),
    // Grab necessary material properties
    _mobility(getADMaterialProperty<Real>("mobility"))

{
}


ADReal
ADCoupledDiffusion::precomputeQpResidual()
{
  return  _mobility[_qp] * _voltage_gradSec[_qp].tr() * _u[_qp];
}
