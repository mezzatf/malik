#include "ADSource.h"

registerMooseObject("malikApp", ADSource);

InputParameters
ADSource::validParams()
{
  InputParameters params = ADKernel::validParams();
  params.addRequiredCoupledVar("charge_density", "The variable representing the charge_density.");
  return params;
}

ADSource::ADSource(const InputParameters & parameters)
  : ADKernel(parameters),
    // Couple to the gradient of the charge_density
    _charge_density(adCoupledValue("charge_density"))
{
}

ADReal
ADSource::computeQpResidual()
{
  // See also: E. Majchrzak and L. Turchan, "The Finite Difference
  // Method For Transient Convection Diffusion", Scientific Research
  // of the Institute of Mathematics and Computer Science, vol. 1,
  // no. 11, 2012, pp. 63-72.
  // http://srimcs.im.pcz.pl/2012_1/art_07.pdf

  // http://en.wikipedia.org/wiki/Superficial_velocity
  return _test[_i][_qp] * - _charge_density[_qp];

}
