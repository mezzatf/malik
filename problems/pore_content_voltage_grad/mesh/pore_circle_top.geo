w   = 1;     // m
h   = 1;     // m

dx0 = 0.005;  // m
dx1 = 0.005;  // m

r   = 0.005; // m

Point(1) = {0, 0, 0, dx1};
Point(2) = {w, 0, 0, dx1};
Point(3) = {w, h, 0, dx0};
Point(4) = {0.705*w, h, 0, 0.005*w};
Point(5) = {0.700*w, h, 0, 0.005*w};
Point(6) = {0.300*w, h, 0, 0.005*w};
Point(7) = {0.295*w, h, 0, 0.005*w};
Point(8) = {0, h, 0, dx0};
//+
Line(1) = {1, 2};
//+
Line(2) = {2, 3};
//+
Line(3) = {3, 4};
//+
Line(4) = {4, 5};
//+
Line(5) = {5, 6};
//+
Line(6) = {6, 7};
//+
Line(7) = {7, 8};
//+
Line(8) = {8, 1};
//+
Curve Loop(1) = {8, 1, 2, 3, 4, 5, 6, 7};
//+
Plane Surface(1) = {1};
//+
Physical Curve("wall") = {8, 1, 2, 3, 7};
//+
Physical Curve("HV") = {6};
//+
Physical Curve("InterElectrode") = {5};
//+
Physical Curve("GE") = {4};
//+
Physical Surface("rock") = {1};
