w   = 1;     // m
h   = 1;     // m

dx0 = 0.005;  // m
dx1 = 0.01;  // m

r   = 0.005; // m

Point(1) = {0, 0, 0, dx1};
Point(2) = {w, 0, 0, dx1};
Point(3) = {w, h, 0, dx0};
Point(4) = {0.75*w, h, 0, dx0};
Point(5) = {0.70*w, h, 0, dx0};
Point(6) = {0.30*w, h, 0, dx0};
Point(7) = {0.25*w, h, 0, dx0};
Point(8) = {0, h, 0, dx1};
//+
Line(1) = {1, 2};
//+
Line(2) = {2, 3};
//+
Line(3) = {3, 4};
//+
Line(4) = {4, 5};
//+
Line(5) = {5, 6};
//+
Line(6) = {6, 7};
//+
Line(7) = {7, 8};
//+
Line(8) = {8, 1};
//+
Point (9) = {0.50500,0.90000,0,dx0};
Point (10) = {0.50500,0.90032,0,dx0};
Point (11) = {0.50499,0.90063,0,dx0};
Point (12) = {0.50498,0.90095,0,dx0};
Point (13) = {0.50496,0.90126,0,dx0};
Point (14) = {0.50494,0.90157,0,dx0};
Point (15) = {0.50491,0.90188,0,dx0};
Point (16) = {0.50488,0.90219,0,dx0};
Point (17) = {0.50484,0.90250,0,dx0};
Point (18) = {0.50480,0.90280,0,dx0};
Point (19) = {0.50475,0.90311,0,dx0};
Point (20) = {0.50470,0.90340,0,dx0};
Point (21) = {0.50465,0.90370,0,dx0};
Point (22) = {0.50458,0.90399,0,dx0};
Point (23) = {0.50452,0.90428,0,dx0};
Point (24) = {0.50445,0.90456,0,dx0};
Point (25) = {0.50438,0.90484,0,dx0};
Point (26) = {0.50430,0.90511,0,dx0};
Point (27) = {0.50421,0.90538,0,dx0};
Point (28) = {0.50413,0.90565,0,dx0};
Point (29) = {0.50404,0.90590,0,dx0};
Point (30) = {0.50394,0.90616,0,dx0};
Point (31) = {0.50384,0.90640,0,dx0};
Point (32) = {0.50374,0.90664,0,dx0};
Point (33) = {0.50363,0.90687,0,dx0};
Point (34) = {0.50352,0.90710,0,dx0};
Point (35) = {0.50341,0.90732,0,dx0};
Point (36) = {0.50329,0.90753,0,dx0};
Point (37) = {0.50317,0.90773,0,dx0};
Point (38) = {0.50305,0.90793,0,dx0};
Point (39) = {0.50292,0.90812,0,dx0};
Point (40) = {0.50279,0.90830,0,dx0};
Point (41) = {0.50266,0.90847,0,dx0};
Point (42) = {0.50252,0.90863,0,dx0};
Point (43) = {0.50239,0.90879,0,dx0};
Point (44) = {0.50225,0.90894,0,dx0};
Point (45) = {0.50210,0.90907,0,dx0};
Point (46) = {0.50196,0.90920,0,dx0};
Point (47) = {0.50181,0.90932,0,dx0};
Point (48) = {0.50166,0.90943,0,dx0};
Point (49) = {0.50152,0.90953,0,dx0};
Point (50) = {0.50136,0.90962,0,dx0};
Point (51) = {0.50121,0.90970,0,dx0};
Point (52) = {0.50106,0.90977,0,dx0};
Point (53) = {0.50090,0.90984,0,dx0};
Point (54) = {0.50075,0.90989,0,dx0};
Point (55) = {0.50059,0.90993,0,dx0};
Point (56) = {0.50043,0.90996,0,dx0};
Point (57) = {0.50028,0.90998,0,dx0};
Point (58) = {0.50012,0.91000,0,dx0};
Point (59) = {0.49996,0.91000,0,dx0};
Point (60) = {0.49980,0.90999,0,dx0};
Point (61) = {0.49965,0.90997,0,dx0};
Point (62) = {0.49949,0.90995,0,dx0};
Point (63) = {0.49933,0.90991,0,dx0};
Point (64) = {0.49917,0.90986,0,dx0};
Point (65) = {0.49902,0.90981,0,dx0};
Point (66) = {0.49887,0.90974,0,dx0};
Point (67) = {0.49871,0.90966,0,dx0};
Point (68) = {0.49856,0.90958,0,dx0};
Point (69) = {0.49841,0.90948,0,dx0};
Point (70) = {0.49826,0.90938,0,dx0};
Point (71) = {0.49811,0.90926,0,dx0};
Point (72) = {0.49797,0.90914,0,dx0};
Point (73) = {0.49783,0.90900,0,dx0};
Point (74) = {0.49768,0.90886,0,dx0};
Point (75) = {0.49755,0.90871,0,dx0};
Point (76) = {0.49741,0.90855,0,dx0};
Point (77) = {0.49728,0.90839,0,dx0};
Point (78) = {0.49714,0.90821,0,dx0};
Point (79) = {0.49702,0.90802,0,dx0};
Point (80) = {0.49689,0.90783,0,dx0};
Point (81) = {0.49677,0.90763,0,dx0};
Point (82) = {0.49665,0.90742,0,dx0};
Point (83) = {0.49653,0.90721,0,dx0};
Point (84) = {0.49642,0.90699,0,dx0};
Point (85) = {0.49631,0.90676,0,dx0};
Point (86) = {0.49621,0.90652,0,dx0};
Point (87) = {0.49611,0.90628,0,dx0};
Point (88) = {0.49601,0.90603,0,dx0};
Point (89) = {0.49592,0.90578,0,dx0};
Point (90) = {0.49583,0.90551,0,dx0};
Point (91) = {0.49574,0.90525,0,dx0};
Point (92) = {0.49566,0.90498,0,dx0};
Point (93) = {0.49559,0.90470,0,dx0};
Point (94) = {0.49551,0.90442,0,dx0};
Point (95) = {0.49545,0.90413,0,dx0};
Point (96) = {0.49538,0.90385,0,dx0};
Point (97) = {0.49533,0.90355,0,dx0};
Point (98) = {0.49527,0.90325,0,dx0};
Point (99) = {0.49522,0.90295,0,dx0};
Point (100) = {0.49518,0.90265,0,dx0};
Point (101) = {0.49514,0.90235,0,dx0};
Point (102) = {0.49510,0.90204,0,dx0};
Point (103) = {0.49508,0.90173,0,dx0};
Point (104) = {0.49505,0.90142,0,dx0};
Point (105) = {0.49503,0.90110,0,dx0};
Point (106) = {0.49502,0.90079,0,dx0};
Point (107) = {0.49501,0.90047,0,dx0};
Point (108) = {0.49500,0.90016,0,dx0};
Point (109) = {0.49500,0.89984,0,dx0};
Point (110) = {0.49501,0.89953,0,dx0};
Point (111) = {0.49502,0.89921,0,dx0};
Point (112) = {0.49503,0.89890,0,dx0};
Point (113) = {0.49505,0.89858,0,dx0};
Point (114) = {0.49508,0.89827,0,dx0};
Point (115) = {0.49510,0.89796,0,dx0};
Point (116) = {0.49514,0.89765,0,dx0};
Point (117) = {0.49518,0.89735,0,dx0};
Point (118) = {0.49522,0.89705,0,dx0};
Point (119) = {0.49527,0.89675,0,dx0};
Point (120) = {0.49533,0.89645,0,dx0};
Point (121) = {0.49538,0.89615,0,dx0};
Point (122) = {0.49545,0.89587,0,dx0};
Point (123) = {0.49551,0.89558,0,dx0};
Point (124) = {0.49559,0.89530,0,dx0};
Point (125) = {0.49566,0.89502,0,dx0};
Point (126) = {0.49574,0.89475,0,dx0};
Point (127) = {0.49583,0.89449,0,dx0};
Point (128) = {0.49592,0.89422,0,dx0};
Point (129) = {0.49601,0.89397,0,dx0};
Point (130) = {0.49611,0.89372,0,dx0};
Point (131) = {0.49621,0.89348,0,dx0};
Point (132) = {0.49631,0.89324,0,dx0};
Point (133) = {0.49642,0.89301,0,dx0};
Point (134) = {0.49653,0.89279,0,dx0};
Point (135) = {0.49665,0.89258,0,dx0};
Point (136) = {0.49677,0.89237,0,dx0};
Point (137) = {0.49689,0.89217,0,dx0};
Point (138) = {0.49702,0.89198,0,dx0};
Point (139) = {0.49714,0.89179,0,dx0};
Point (140) = {0.49728,0.89161,0,dx0};
Point (141) = {0.49741,0.89145,0,dx0};
Point (142) = {0.49755,0.89129,0,dx0};
Point (143) = {0.49768,0.89114,0,dx0};
Point (144) = {0.49783,0.89100,0,dx0};
Point (145) = {0.49797,0.89086,0,dx0};
Point (146) = {0.49811,0.89074,0,dx0};
Point (147) = {0.49826,0.89062,0,dx0};
Point (148) = {0.49841,0.89052,0,dx0};
Point (149) = {0.49856,0.89042,0,dx0};
Point (150) = {0.49871,0.89034,0,dx0};
Point (151) = {0.49887,0.89026,0,dx0};
Point (152) = {0.49902,0.89019,0,dx0};
Point (153) = {0.49917,0.89014,0,dx0};
Point (154) = {0.49933,0.89009,0,dx0};
Point (155) = {0.49949,0.89005,0,dx0};
Point (156) = {0.49965,0.89003,0,dx0};
Point (157) = {0.49980,0.89001,0,dx0};
Point (158) = {0.49996,0.89000,0,dx0};
Point (159) = {0.50012,0.89000,0,dx0};
Point (160) = {0.50028,0.89002,0,dx0};
Point (161) = {0.50043,0.89004,0,dx0};
Point (162) = {0.50059,0.89007,0,dx0};
Point (163) = {0.50075,0.89011,0,dx0};
Point (164) = {0.50090,0.89016,0,dx0};
Point (165) = {0.50106,0.89023,0,dx0};
Point (166) = {0.50121,0.89030,0,dx0};
Point (167) = {0.50136,0.89038,0,dx0};
Point (168) = {0.50152,0.89047,0,dx0};
Point (169) = {0.50166,0.89057,0,dx0};
Point (170) = {0.50181,0.89068,0,dx0};
Point (171) = {0.50196,0.89080,0,dx0};
Point (172) = {0.50210,0.89093,0,dx0};
Point (173) = {0.50225,0.89106,0,dx0};
Point (174) = {0.50239,0.89121,0,dx0};
Point (175) = {0.50252,0.89137,0,dx0};
Point (176) = {0.50266,0.89153,0,dx0};
Point (177) = {0.50279,0.89170,0,dx0};
Point (178) = {0.50292,0.89188,0,dx0};
Point (179) = {0.50305,0.89207,0,dx0};
Point (180) = {0.50317,0.89227,0,dx0};
Point (181) = {0.50329,0.89247,0,dx0};
Point (182) = {0.50341,0.89268,0,dx0};
Point (183) = {0.50352,0.89290,0,dx0};
Point (184) = {0.50363,0.89313,0,dx0};
Point (185) = {0.50374,0.89336,0,dx0};
Point (186) = {0.50384,0.89360,0,dx0};
Point (187) = {0.50394,0.89384,0,dx0};
Point (188) = {0.50404,0.89410,0,dx0};
Point (189) = {0.50413,0.89435,0,dx0};
Point (190) = {0.50421,0.89462,0,dx0};
Point (191) = {0.50430,0.89489,0,dx0};
Point (192) = {0.50438,0.89516,0,dx0};
Point (193) = {0.50445,0.89544,0,dx0};
Point (194) = {0.50452,0.89572,0,dx0};
Point (195) = {0.50458,0.89601,0,dx0};
Point (196) = {0.50465,0.89630,0,dx0};
Point (197) = {0.50470,0.89660,0,dx0};
Point (198) = {0.50475,0.89689,0,dx0};
Point (199) = {0.50480,0.89720,0,dx0};
Point (200) = {0.50484,0.89750,0,dx0};
Point (201) = {0.50488,0.89781,0,dx0};
Point (202) = {0.50491,0.89812,0,dx0};
Point (203) = {0.50494,0.89843,0,dx0};
Point (204) = {0.50496,0.89874,0,dx0};
Point (205) = {0.50498,0.89905,0,dx0};
Point (206) = {0.50499,0.89937,0,dx0};
Point (207) = {0.50500,0.89968,0,dx0};
Point (208) = {0.50500,0.90000,0,dx0};
//+
BSpline(9)={9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,167,168,169,170,171,172,173,174,175,176,177,178,179,180,181,182,183,184,185,186,187,188,189,190,191,192,193,194,195,196,197,198,199,200,201,202,203,204,205,206,207,208,9};
//+
Physical Curve("wall") = {7, 8, 1, 2, 3, 5};
//+
Physical Curve("HV") = {6};
//+
Physical Curve("GE") = {4};
//+
Curve Loop(1) = {7, 8, 1, 2, 3, 4, 5, 6};
//+
Curve Loop(2) = {9};
//+
Plane Surface(1) = {1, 2};
//+
Plane Surface(2) = {2};
//+
Physical Surface("pore") = {2};
//+
Physical Surface("rock") = {1};
