ls = 5;

w  = 100; // mm
h  = 20;  // mm
R  = 0.5;   // mm
dx0 = 0.1;
dx1 = 0.2;
dx2 = 5;

Point(1) = {0, 0, 0, dx2};
Point(2) = {w, 0, 0, dx2};

Point(3) = {25,20,0,dx1};
Point(4) = {30,20,0,dx1};
Point(5) = {70,20,0,dx1};
Point(6) = {75,20,0,dx1};

Point(7) = {w, h, 0, dx2};
Point(8) = {0, h, 0, dx2};


Point(9)  = {40,16.5,0,dx0};
Point(10) = {40,17,0,dx0};
Point(11) = {40,17.5,0,dx0};

Point(12) = {50,16.5,0,dx0};
Point(13) = {50,17,0,dx0};
Point(14) = {50,17.5,0,dx0};

Point(15) = {60,16.5,0,dx0};
Point(16) = {60,17,0,dx0};
Point(17) = {60,17.5,0,dx0};
//+
Line(1) = {1, 2};
//+
Line(2) = {2, 7};
//+
Line(3) = {7, 6};
//+
Line(4) = {6, 5};
//+
Line(5) = {5, 4};
//+
Line(6) = {4, 3};
//+
Line(7) = {3, 8};
//+
Line(8) = {8, 1};
//+
Circle(9) = {11, 10, 9};
//+
Circle(10) = {9, 10, 11};
//+
Circle(11) = {14, 13, 12};
//+
Circle(12) = {12, 13, 14};
//+
Circle(13) = {17, 16, 15};
//+
Circle(14) = {15, 16, 17};
//+
Curve Loop(1) = {7, 8, 1, 2, 3, 4, 5, 6};
//+
Curve Loop(2) = {9, 10};
//+
Curve Loop(3) = {11, 12};
//+
Curve Loop(4) = {13, 14};
//+
Plane Surface(1) = {1, 2, 3, 4};
//+
Plane Surface(2) = {2};
//+
Plane Surface(3) = {3};
//+
Plane Surface(4) = {4};
//+
Physical Curve("all", 15) = {7, 8, 1, 2, 3, 5};
//+
Physical Curve("HV", 16) = {6};
//+
Physical Curve("GE", 17) = {4};
//+
Physical Surface("rock", 18) = {1};
//+
Physical Surface("pore", 19) = {2, 3, 4};
