[Mesh]
  [./gmg]
    type= GeneratedMeshGenerator
    dim  = 2
    nx   = 200
    ny   = 40
    xmax = 100
    ymax = 20
  []
  [./HVNode]
    type = BoundingBoxNodeSetGenerator
    input = gmg
    bottom_left  = '25  20 0.0'
    top_right    = '30  20 0.0'
    new_boundary = 101
  []
  [./GENode]
    type = BoundingBoxNodeSetGenerator
    input = HVNode
    bottom_left  = '70  20 0.0'
    top_right    = '75  20 0.0'
    new_boundary = 102
  []
[]
[AuxVariables]
  [EField_x]
    family = MONOMIAL
     order = FIRST
  []
  [EField_y]
    family = MONOMIAL
     order = FIRST
  []
[]

[Variables]
  [voltage]
  []
[]


[Kernels]
  [voltage_diffusion]
    type = ADHeatConduction
    variable = voltage
    thermal_conductivity = electric_permittivity
  []
[]

[AuxKernels]
   [E_Fieldx]
     type      = VariableGradientComponent
     component = x
     variable  = EField_x
     gradient_variable = voltage
   []
   [E_Fieldy]
     type      = VariableGradientComponent
     component = y
     variable  = EField_y
     gradient_variable = voltage
   []
[]


[Problem]
  type = FEProblem
[]

[BCs]
  [all_voltage]
    type     = NeumannBC
    variable = voltage
    boundary = 'top bottom left right'
    value    = 0
  []
  [HV_voltage]
    type     =  FunctionDirichletBC
    variable =  voltage
    boundary =  101
    function =  voltage_pulse
  []
  [GE_voltage]
    type     = DirichletBC
    variable = voltage
    boundary = 102
    value    = 0
  []
[]

[Executioner]
  type  = Transient
  end_time = 0.5e-6
  num_steps = 10
  solve_type = 'PJFNK'
  petsc_options_iname = '-pc_type -pc_hypre_type'
  petsc_options_value = 'hypre boomeramg'
[]

[Functions]
  [voltage_pulse]
    type  = ParsedFunction
    value = '1e5*(tanh(t*9e6)-sin(2.0*pi*t/(8.0*1e-6)))/0.8'
    # value = 1e5
  []
[]


[Materials]
  [rock]
    type = ADGenericConstantMaterial
    block  = 0
    prop_names  = 'diffusivity specific_heat density mobility electric_permittivity'
    prop_values = '10e7 1 1 5 10'
    outputs = exodus
  []
  # [pore-air]
  #   type = ADGenericConstantMaterial
  #   block  = 1
  #   prop_names  = 'diffusivity specific_heat density mobility electric_permittivity'
  #   prop_values = '10e7 1 1 5 1'
  #   outputs = exodus
  # []
  # [pore-water]
  #   type = ADGenericConstantMaterial
  #   block  = 2
  #   prop_names  = 'diffusivity specific_heat density mobility electric_permittivity'
  #   prop_values = '10e7 1 1 5 100'
  #   outputs = exodus
  # []
[]

[Outputs]
  exodus = true
[]
