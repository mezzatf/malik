[Mesh]
  [./gmg]
    type= GeneratedMeshGenerator
    dim  = 2
    nx   = 100
    ny   = 20
    xmax = 100
    ymax = 20
  []
  [./HVNode]
    type = BoundingBoxNodeSetGenerator
    input = gmg
    bottom_left  = '25  20 0.0'
    top_right    = '30  20 0.0'
    new_boundary = 101
  []
  [./GENode]
    type = BoundingBoxNodeSetGenerator
    input = HVNode
    bottom_left  = '70  20 0.0'
    top_right    = '75  20 0.0'
    new_boundary = 102
  []
  [./Pore]
    type = SubdomainBoundingBoxGenerator
    input = GENode
    bottom_left  = '35  15 0.0'
    top_right    = '45  18 0.0'
    block_id = 1
  []
[]

[Variables]
  [voltage]
  []
  # [charge_density]
  # []
[]


[Kernels]
  [voltage_diffusion]
    type = ADHeatConduction
    variable = voltage
    thermal_conductivity = electric_permittivity
  []
  # [./source]
  #   type = ADSource
  #   variable = voltage
  #   charge_density = charge_density
  # [../]
  # [charge_diffusion]
  #   type  = ADHeatConduction
  #   variable = charge_density
  #   thermal_conductivity = diffusivity
  # []
  # [charge_rate]
  #   type = ADHeatConductionTimeDerivative
  #   variable = charge_density
  #   specific_heat = specific_heat
  #   density = density
  # []
  # [charge_advection]
  #   type = ADCoupledConvection
  #   variable = charge_density
  #   voltage = voltage
  # []

[]

[Problem]
  type = FEProblem
[]

[BCs]
  [all_voltage]
    type     = NeumannBC
    variable = voltage
    boundary = 'top bottom left right'
    value    = 1000
  []
  [HV_voltage]
    type     =  FunctionDirichletBC
    variable =  voltage
    boundary =  101
    function =  voltage_pulse
  []
  [GE_voltage]
    type     = DirichletBC
    variable = voltage
    boundary = 102
    value    = 0
  []

  # [all]
  #   type = NeumannBC
  #   variable = charge_density
  #   boundary = 'left right bottom top'
  #   value = 100
  # []
  # [HV_charge_density]
  #   # type = HeatConductionOutflow
  #   type = DirichletBC
  #   variable = charge_density
  #   boundary = 101
  #   value = 10
  # []
  # [GE_charge_density]
  #   type  = DirichletBC
  #   variable = charge_density
  #   boundary = 102
  #   value = 0
  # []
[]

[Executioner]
  type  = Transient
  num_steps = 50
  end_time = 1e-6
  solve_type = 'PJFNK'
  petsc_options_iname = '-pc_type -pc_hypre_type'
  petsc_options_value = 'hypre boomeramg'
[]


[Functions]
  [voltage_pulse]
    type  = ParsedFunction
    value = '3e5*(tanh(t*9e6)-sin(2.0*pi*t/(8.0*1e-6)))/0.8'
  []
[]


[Materials]
  [rock]
    type = ADGenericConstantMaterial
    block  = 0
    prop_names  = 'diffusivity specific_heat density mobility electric_permittivity'
    prop_values = '10e7 1 1 5 10'
  []
  [void]
    type = ADGenericConstantMaterial
    block  = 1
    prop_names  = 'diffusivity specific_heat density mobility electric_permittivity'
    prop_values = '10e7 1 1 5 1'

  []
[]

[Outputs]
  exodus = true
[]
