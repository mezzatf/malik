[Mesh]
  [./gmg]
    type = FileMeshGenerator
    file = "test.msh"
  []
[]

[Variables]
  [voltage]
  []
  [charge_density]
  []
[]


[Kernels]
  [voltage_diffusion]
    type = ADHeatConduction
    variable = voltage
    thermal_conductivity = electric_permittivity
  []
  [./source]
    type = ADSource
    variable = voltage
    charge_density = charge_density
  [../]

  [charge_diffusion]
    type  = ADHeatConduction
    variable = charge_density
    thermal_conductivity = diffusivity
  []
  [charge_rate]
    type = ADHeatConductionTimeDerivative
    variable = charge_density
    specific_heat = specific_heat
    density = density
  []
  [charge_advection]
    type = ADCoupledConvection
    variable = charge_density
    voltage = voltage
  []
  [charge_source]
    type = ADCoupledDiffusion
    variable = charge_density
    voltage = voltage
  []
[]

[Problem]
  type = FEProblem
[]

[BCs]
  [all_voltage]
    type     = NeumannBC
    variable = voltage
    boundary = all
    value    = 1000
  []
  [HV_voltage]
    type     =  FunctionDirichletBC
    variable =  voltage
    boundary =  HV
    function =  voltage_pulse
  []
  [GE_voltage]
    type     = DirichletBC
    variable = voltage
    boundary = GE
    value    = 0
  []

  [all]
    type = NeumannBC
    variable = charge_density
    boundary = all
    value = 100
  []
  [HV_charge_density]
    # type = HeatConductionOutflow
    type = DirichletBC
    variable = charge_density
    boundary = HV
    value = 10
  []
  [GE_charge_density]
    type  = DirichletBC
    variable = charge_density
    boundary = GE
    value = 0
  []
[]

[AuxVariables]
  [EField_x]
    family = MONOMIAL
     order = FIRST
  []
  [EField_y]
    family = MONOMIAL
     order = FIRST
  []
[]

[AuxKernels]
   [E_Fieldx]
     type      = VariableGradientComponent
     component = x
     variable  = EField_x
     gradient_variable = voltage
   []
   [E_Fieldy]
     type      = VariableGradientComponent
     component = y
     variable  = EField_y
     gradient_variable = voltage
   []
[]


[Executioner]
  type  = Transient
  num_steps = 10
  end_time = 0.5e-6
  solve_type = 'PJFNK'
  petsc_options_iname = '-pc_type -pc_hypre_type'
  petsc_options_value = 'hypre boomeramg'
[]


[Functions]
  [voltage_pulse]
    type  = ParsedFunction
    value = '3e5*(tanh(t*9e6)-sin(2.0*pi*t/(8.0*1e-6)))/0.8'
  []
[]


[Materials]
  [rock]
    type = ADGenericConstantMaterial
    block  = 18
    prop_names  = 'diffusivity specific_heat density mobility electric_permittivity'
    prop_values = '10e7 1 1 5 10'
    outputs = exodus
  []
  [void]
    type = ADGenericConstantMaterial
    block  = 19
    prop_names  = 'diffusivity specific_heat density mobility electric_permittivity'
    prop_values = '10e7 1 1 5 80'
    outputs = exodus

  []
[]

[Outputs]
  exodus = true
[]
