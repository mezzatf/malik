[Mesh]
  [./gmg]
    type= GeneratedMeshGenerator
    dim  = 2
    nx   = 100
    ny   = 20
    xmax = 100
    ymax = 20
  []
  [./HVNode]
    type = BoundingBoxNodeSetGenerator
    input = gmg
    bottom_left  = '25  20 0.0'
    top_right    = '30  20 0.0'
    new_boundary = 101
  []
  [./GENode]
    type = BoundingBoxNodeSetGenerator
    input = HVNode
    bottom_left  = '70  20 0.0'
    top_right    = '75  20 0.0'
    new_boundary = 102
  []
[]

[Variables]
  # [voltage]
  # []
  [charge_density]
  []
[]


[Kernels]
  # [voltage_diffusion]
  #   type = ADHeatConduction
  #   variable = voltage
  #   thermal_conductivity = electric_permittivity
  # []
  # [./source]
  #   type = ADSource
  #   variable = voltage
  #   charge_density = charge_density
  # [../]
  [charge_diffusion]
    type = ADHeatConduction
    variable = charge_density
    thermal_conductivity = diffusivity
  []
  [charge_rate]
    type = ADHeatConductionTimeDerivative
    variable = charge_density
    specific_heat = specific_heat
    density = density
  []
  # [charge_advection]
  #   type = ADCoupledConvection
  #   variable = charge_density
  #   voltage = voltage
  # []
[]


[BCs]
  # [all_voltage]
  #   type     = NeumannBC
  #   variable = voltage
  #   boundary = 'top bottom left right'
  #   value    = 1000
  # []
  # [HV_voltage]
  #   type     =  FunctionDirichletBC
  #   variable =  voltage
  #   boundary =  101
  #   function =  voltage_pulse
  # []
  # [GE_voltage]
  #   type     = DirichletBC
  #   variable = voltage
  #   boundary = 102
  #   value    = 0
  # []

  [all_charge_density]
    type  = NeumannBC
    variable = charge_density
    boundary = 'top bottom left right'
    value = 300
  []
  [HV_charge_density]
    # type = HeatConductionOutflow
    type = DirichletBC
    variable = charge_density
    boundary = 101
    value = 350
  []
  [GE_charge_density]
    type  = DirichletBC
    variable = charge_density
    boundary = 102
    value = 0
  []
[]

[Executioner]
  type  = Transient
  dt    = 7.37464e-08 # 10 timesteps per period
  end_time = 1e-6 # 10 periods
  nl_rel_tol = 1e-07
  solve_type = 'PJFNK'
  petsc_options_iname = '-pc_type -pc_hypre_type'
  petsc_options_value = 'hypre boomeramg'
[]


[Functions]
  # [voltage_pulse]
  #   type  = ParsedFunction
  #   value = '1e5*(tanh(t*9e6)-sin(2.0*pi*t/(8.0*1e-6)))/0.8'
  # []

[]
[Materials]
  [rock]
    type = ADGenericConstantMaterial
    prop_names  = 'specific_heat density diffusivity'
    prop_values = '1 200 18 '

  []
[]

[Outputs]
  exodus = true
[]
