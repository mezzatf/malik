##
## Phase maps
##
## A number of different phases (a, b, c, d, and so on) are defined
## to map material (mineral) properties for mechanical and thermal properties
## The phases are mapped according to loaded image functions (e.g., thin sections)
### phase ranges
it_a_lo = -1.0e3
it_a_hi = 102.0
it_b_lo = 102.0
it_b_hi = 206.0
it_c_lo = 206.0
it_c_hi = 254.5
it_d_lo = 254.5
it_d_hi = 9120.0
it_e_lo = 9120.0
it_e_hi = 9160.0
it_f_lo = 9160.0
it_f_hi = 9360.0
it_z_lo = 9360.0
it_z_hi = 9390.0
###
### material properties
###
# density - rho
# specific heat - cp
# thermal conductivity - k
### GRANITE
# a - K-Feldspar
# b - Plagioclase
# c - Quartz
# d - Biotite
# e - averaged granite eberhardt 1999
# f - averaged granodiorite eberhardt 1999
## density
GR_rho_a = 3000.0
GR_rho_b = 3100.0
GR_rho_c = 3200.0
GR_rho_d = 3300.0
GR_rho_e = 3400.0
GR_rho_f = 3500.0
GR_rho_z = 3600.0
## bulk modulus
GR_E_a = 50.0e3
GR_E_b = 51.0e3
GR_E_c = 52.0e3
GR_E_d = 53.0e3
GR_E_e = 54.0e3
GR_E_f = 55.0e3
GR_E_z = 56.0e3
## poisson ratio
GR_eps_a = 0.20
GR_eps_b = 0.21
GR_eps_c = 0.22
GR_eps_d = 0.23
GR_eps_e = 0.24
GR_eps_f = 0.25
GR_eps_z = 0.26
### strain
GR_e_a = 1.0e-6
GR_e_b = 1.1e-6
GR_e_c = 1.2e-6
GR_e_d = 1.3e-6
GR_e_e = 1.4e-6
GR_e_f = 1.5e-6
GR_e_z = 1.6e-6
## specific heat
GR_cp_a = 1000.0
GR_cp_b = 1100.0
GR_cp_c = 1200.0
GR_cp_d = 1300.0
GR_cp_e = 1400.0
GR_cp_f = 1500.0
GR_cp_z = 1600.0
## electrical conductivity
GR_elK_a = 1.0e-14
GR_elK_b = 2.0e-14
GR_elK_c = 3.0e-14
GR_elK_d = 4.0e-14
GR_elK_e = 5.0e-14
GR_elK_f = 6.0e-14
GR_elK_z = 7.0e-14
## thermal conductivity
GR_thK_a = 2.0
GR_thK_b = 2.1
GR_thK_c = 2.2
GR_thK_d = 2.3
GR_thK_e = 2.4
GR_thK_f = 2.5
GR_thK_z = 2.6
## porosity
### plagioclase (montgomerey and brace) -
###### westerley granite (0.9%) - plagioclase (1.8%)
GR_vf_a = 0.010
GR_vf_b = 0.011
GR_vf_c = 0.012
GR_vf_d = 0.013
GR_vf_e = 0.014
GR_vf_f = 0.015
GR_vf_z = 0.016
### WATER (damaged material - drilling fluid)
WA_rho_z = 1000.0
WA_cp_z  = 4181.0
WA_thK_z = 0.591
WA_E_z = 2.0e3
WA_e_z = 1.0e-8
###
### SOLVER
###
time_step = 1.0e-7
time_end = 2.0e-7
plot_intervals = 4
###
### TEMPERATURE
###
T_solid_initial = 300.0
T_fluid_initial = ${T_solid_initial}
T_mix_initial = ${T_solid_initial}
T_fluid_reference = 423.0
###
### FLUID
###
density_fluid_constant = 1000
specific_heat_fluid_constant = 4100
concentration_fluid = 0.2
###
### VOLTAGE
###
voltage_high = 0.60e6
voltage_low = 0.0



[Mesh]
  type = GeneratedMesh
  dim = 2
  nx = 500
  ny = 100
  nz = 0
  xmin = 0.0
  xmax = 0.1
  ymax = 0.02
  zmin = 0
  zmax = 0
  elem_type = QUAD4
[]



[Variables]
  ### electric potential
  [./phi]
    initial_condition = 0.0
  [../]
[]


[Kernels]
#  [./elcond]
#    type = HeatConduction
#    variable = phi
#    diffusion_coefficient = electrical_conductivity_material_mix
#  [../]
  # [./htcond]
  #   type = HeatConduction
  #   variable = T
  # [../]
  # [./time_deriv_T]
  #   type = SpecificHeatConductionTimeDerivative
  #   specific_heat_solid = specific_heat_solid
  #   variable = phi_solid
  # [../]
[]


[AuxVariables]
  ###
  ### PARAMETER FIELD
  ###
  [./map]
    order = CONSTANT
    family = MONOMIAL
  [../]
  [./volumetric_fraction_fluid]
    order = CONSTANT
    family = MONOMIAL
  [../]
  [./specific_heat_fluid]
    order = CONSTANT
    family = MONOMIAL
  [../]
  [./density_fluid]
    order = CONSTANT
    family = MONOMIAL
  [../]
  ###
  ### TEMPERATURE
  ###
  ### SOLID
  [./T_solid]
    order = CONSTANT
    family = MONOMIAL
  [../]
  [./T_solid_delta]
    order = CONSTANT
    family = MONOMIAL
  [../]
  ### FLUID
  [./T_fluid]
    order = CONSTANT
    family = MONOMIAL
  [../]
  [./T_fluid_delta]
    order = CONSTANT
    family = MONOMIAL
  [../]
[]



[AuxKernels]
  ###
  ### VOLUMETRIC FRACTION
  ###
  [./volumetric_fraction_fluid]
    type = MaterialRealAux
    property = volumetric_fraction_material_fluid
    variable = volumetric_fraction_fluid
  [../]
  [./specific_heat_fluid]
    type = MaterialRealAux
    property = specific_heat_material_fluid
    variable = specific_heat_fluid
  [../]
  [./density_fluid]
    type = MaterialRealAux
    property = density_material_fluid
    variable = density_fluid
  [../]
[]



[Functions]
  ###
  ### IMAGE FUNCTION
  ###
  [./my_image]
    type = ImageFunction
    file_base = grain_matrix
    file_suffix = png
    file_range = '00'
  [../]
[]


[ICs]
  ###
  ### FIELD MAP (initialize read in image)
  ###
  [./map_ic]
    type = FunctionIC
    function = my_image
    variable = map
  [../]
[]


[BCs]
  ###
  ### PHI
  ###
  ### top BC
  [./phi_top_dirichlet]
    type = DirichletBC
    boundary = 'top'
    variable = phi
    value = ${voltage_high}
  [../]
  ### bottom BC
  [./phi_bottom_dirichlet]
    type = DirichletBC
    boundary = 'bottom'
    variable = phi
    value = ${voltage_low}
  [../]
[]



[Materials]
  ###
  ### SPECIFIC HEAT
  ###
  ### SOLID
  [./phasemap_cp_solid]
    type = ParsedMaterial
    f_name = specific_heat_solid
    args = 'map'
    function = 'if(map>${it_z_lo},${GR_cp_z},if(map>${it_f_lo},${GR_cp_f},if(map>${it_e_lo},
    ${GR_cp_e},if(map>${it_d_lo},${GR_cp_d},if(map>${it_c_lo},${GR_cp_c},if(map>${it_b_lo},
    ${GR_cp_b},if(map>${it_a_lo},${GR_cp_a},${GR_cp_z})))))))'
    outputs = exodus
  [../]
  ### FLUID
  [./phasemap_cp_fluid]
    type = ParsedMaterial
    f_name = specific_heat_material_fluid
    args = 'map'
    function = '${specific_heat_fluid_constant}'
    outputs = exodus
  [../]
  ###
  ### DENSITY
  ###
  ### SOLID
  [./phasemap_rho_solid]
    type = ParsedMaterial
    f_name = density_solid
    args = 'map'
    function = 'if(map>${it_z_lo},${GR_rho_z},if(map>${it_f_lo},${GR_rho_f},if(map>${it_e_lo},
    ${GR_rho_e},if(map>${it_d_lo},${GR_rho_d},if(map>${it_c_lo},${GR_rho_c},if(map>${it_b_lo},
    ${GR_rho_b},if(map>${it_a_lo},${GR_rho_a},${GR_rho_z})))))))'
    outputs = exodus
  [../]
  ### FLUID
  [./phasemap_rho_fluid]
    type = ParsedMaterial
    f_name = density_material_fluid
    args = 'map'
    function = '${density_fluid_constant}'
    outputs = exodus
  [../]
  ### phase field based volumetric fluid fraction
  [./volumetric_fraction_material_fluid]
    type = ParsedMaterial
    f_name = volumetric_fraction_material_fluid
    args = 'map'
    function = 'if(map>${it_z_lo},${GR_vf_z},if(map>${it_f_lo},${GR_vf_f},if(map>${it_e_lo},
    ${GR_vf_e},if(map>${it_d_lo},${GR_vf_d},if(map>${it_c_lo},${GR_vf_c},if(map>${it_b_lo},
    ${GR_vf_b},if(map>${it_a_lo},${GR_vf_a},${GR_vf_z})))))))'
    outputs = exodus
  [../]
  ### define material class based on material phases
  [./F_a]
    type = DerivativeParsedMaterial
    f_name = F_a
    function = 'if(${it_a_lo}<map,if(map<=${it_a_hi},1,0),0)'
    args = map
    outputs = exodus
  [../]
  [./F_b]
    type = DerivativeParsedMaterial
    f_name = F_b
    function = 'if(${it_b_lo}<map,if(map<=${it_b_hi},1,0),0)'
    args = map
    outputs = exodus
  [../]
  [./F_c]
    type = DerivativeParsedMaterial
    f_name = F_c
    function = 'if(${it_c_lo}<map,if(map<=${it_c_hi},1,0),0)'
    args = map
    outputs = exodus
  [../]
  [./F_d]
    type = DerivativeParsedMaterial
    f_name = F_d
    function = 'if(${it_d_lo}<map,if(map<=${it_d_hi},1,0),0)'
    args = map
    outputs = exodus
  [../]
  [./F_e]
    type = DerivativeParsedMaterial
    f_name = F_e
    function = 'if(${it_e_lo}<map,if(map<=${it_e_hi},1,0),0)'
    args = map
    outputs = exodus
  [../]
  [./F_f]
    type = DerivativeParsedMaterial
    f_name = F_f
    function = 'if(${it_f_lo}<map,if(map<=${it_f_hi},1,0),0)'
    args = map
    outputs = exodus
  [../]
  [./F_damaged]
    type = DerivativeParsedMaterial
    f_name = F_damaged
    function = 'if(${it_z_lo}<map,if(map<=${it_z_hi},1,0),0)'
    args = map
    outputs = exodus
  [../]
  [./F_damaged_b]
    type = DerivativeParsedMaterial
    f_name = F_damaged_b
    function = 'if(map<${it_a_lo},1,0)'
    args = map
    outputs = exodus
  [../]
  ### materials for factors multiplied onto the "per Kelvin tensors"
  [./func_a]
    type = DerivativeParsedMaterial
    function = 'if(${it_a_lo}<map,if(map<=${it_a_hi},map,0),0)'
    f_name = f_a
    args = 'map'
  [../]
  [./func_b]
    type = DerivativeParsedMaterial
    function = 'if(${it_b_lo}<map,if(map<=${it_b_hi},map,0),0)'
    f_name = f_b
    args = 'map'
  [../]
  [./func_c]
    type = DerivativeParsedMaterial
    function = 'if(${it_c_lo}<map,if(map<=${it_c_hi},map,0),0)'
    f_name = f_c
    args = 'map'
  [../]
  [./func_d]
    type = DerivativeParsedMaterial
    function = 'if(${it_d_lo}<map,if(map<=${it_d_hi},map,0),0)'
    f_name = f_d
    args = 'map'
  [../]
  [./func_e]
    type = DerivativeParsedMaterial
    function = 'if(${it_e_lo}<map,if(map<=${it_e_hi},map,0),0)'
    f_name = f_e
    args = 'map'
  [../]
  [./func_f]
    type = DerivativeParsedMaterial
    function = 'if(${it_f_lo}<map,if(map<=${it_f_hi},map,0),0)'
    f_name = f_f
    args = 'map'
  [../]
  [./func_damaged]
    type = DerivativeParsedMaterial
    function = 'if(${it_z_lo}<map,if(map<=${it_z_hi},map,0),0)'
    f_name = f_damaged
    args = 'map'
  [../]
  [./func_damaged_b]
    type = DerivativeParsedMaterial
    function = 'if(map<${it_a_lo},map,0)'
    f_name = f_damaged_b
    args = 'map'
  [../]
[]



[Executioner]
  type = Transient
  solve_type = NEWTON

  petsc_options = ksp_monitor
  petsc_options_iname = '-pc_type -pc_factor_mat_solver_package'
  petsc_options_value = 'lu mumps'

  nl_rel_tol = 1e-5
  nl_abs_tol = 1E-10

  dt = ${time_step}
  end_time = ${time_end}
[]



[Debug]
  show_var_residual_norms = true
[]



[Outputs]
  interval = ${plot_intervals}
  exodus = true
  perf_graph = true
  file_base = example_segmentation
[]
