[Mesh]
  type = GeneratedMesh
  dim  = 2
  nx   = 100
  ny   = 50
  xmax = 1.0
  ymax = 0.5

[]

[Variables]
  [voltage]
  []
[]

[Kernels]
  [VADDiffusion]
    type = VADDiffusion
    variable = voltage
  []
[]

[BCs]
  [all]
     type = NeumannBC
     boundary = 'bottom top'
     variable = voltage
     value =  10
  []
  [Left]
     type = DirichletBC
     boundary = 'left'
     variable = voltage
     value = 100
  []
  [Right]
    type = DirichletBC
    variable = voltage
    boundary = 'right'
    value = 0
  []
[]


[Executioner]
  type = Steady
  solve_type = NEWTON
[]

[Outputs]
  exodus = true
[]
