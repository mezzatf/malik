[Mesh]
  type = GeneratedMesh
  dim  = 2
  nx   = 100
  ny   = 50
  xmax = 1.0
  ymax = 0.5

[]

[Variables]
  [charge_density]
  []
[]

[Kernels]
  [CADDiffusion]
    type = CADDiffusion
    variable = charge_density
    cdiffusivity = 5
  []
[]

[BCs]
  [all]
     type = NeumannBC
     boundary = 'bottom top'
     variable = charge_density
     value =  10
  []
  [Left]
     type = DirichletBC
     boundary = 'left'
     variable = charge_density
     value = 10
  []
  [Right]
    type = DirichletBC
    variable = charge_density
    boundary = 'right'
    value = 0
  []
[]


[Executioner]
  type = Steady
  solve_type = NEWTON
[]

[Outputs]
  exodus = true
[]
