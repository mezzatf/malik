[Mesh]
  [./gmg]
    type= GeneratedMeshGenerator
    dim  = 2
    nx   = 100
    ny   = 20
    xmax = 100
    ymax = 20
  []
  [./HVNode]
    type = BoundingBoxNodeSetGenerator
    input = gmg
    bottom_left  = '25  20 0.0'
    top_right    = '30  20 0.0'
    new_boundary = 101
  []
  [./GENode]
    type = BoundingBoxNodeSetGenerator
    input = HVNode
    bottom_left  = '70  20 0.0'
    top_right    = '75  20 0.0'
    new_boundary = 102
  []
[]

[Variables]
  [charge_density]
  []
[]


[Kernels]
  [charge_diffusion]
    type = ADHeatConduction
    variable = charge_density
    thermal_conductivity = diffusivity
  []
[]

[Problem]
  type = FEProblem
[]

[BCs]
  [HV_charge_density]
    # type = HeatConductionOutflow
    type = DirichletBC
    variable = charge_density
    boundary = 101
    value = 350
  []
  [GE_charge_density]
    type  = DirichletBC
    variable = charge_density
    boundary = 102
    value = 0
  []
[]

[Executioner]
  type  = Steady
  solve_type = 'PJFNK'
  petsc_options_iname = '-pc_type -pc_hypre_type'
  petsc_options_value = 'hypre boomeramg'
[]


[Functions]
[]


[Materials]
  [rock]
    type = ADGenericConstantMaterial
    prop_names  = 'diffusivity'
    prop_values = '18 '

  []
[]

[Outputs]
  exodus = true
[]
