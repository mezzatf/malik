from netCDF4 import Dataset as ns
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from mpl_toolkits.axes_grid1 import make_axes_locatable
import pandas as pd
import math



grads   = ['5MV', '15MV', '30MV']
fluids  = ['air', 'brine20', 'brine10', 'brine02', 'water']
EmaxAir = []
EminWat = []

Data = {}
Data["grad"]  = [5, 15, 30]

gs  = gridspec.GridSpec(len(fluids), len(grads),wspace=0.2)

if len(grads) == 1:
   fig = plt.figure(figsize=(16,3.5))
   bottomspace = 0.2
if len(grads) == 2:
   fig = plt.figure(figsize=(16,5))
   bottomspace = 0.15
if len(grads) == 3:
   fig = plt.figure(figsize=(16,8))
   bottomspace = 0.1

def roundup(x):
    return int(math.ceil(x / 10.0)) * 10

fs = 18
for n, fluid in enumerate(fluids):
    j = n
    E_fluid = []
    for f, grad in enumerate(grads):
        if f==0:
            k = 0
        else:
            k = f+1
            k = f
        file  = 'laplace_%s_circle_%s_out.e'%(fluid, grad)
        data  = ns(file)
        # GET THE X-Coord of nodes
        X = data.variables["coordx"]
        X.set_auto_mask(False)
        X = X [:]
        # GET THE Y-Coord of nodes
        Y = data.variables["coordy"]
        Y.set_auto_mask(False)
        Y = Y [:]
        print(j,k)

        # GET THE NAMES OF THE NODE VARIABLES
        variable_node_names = data.variables["name_nod_var"]
        variable_node_names.set_auto_mask(False)
        point_data_names = [b"".join(c).decode("UTF-8") for c in variable_node_names[:]]
        print(point_data_names)

        for i in np.arange(len(point_data_names)):
            if point_data_names[i] == "EField_x":
               EField_x = data.variables["vals_nod_var%s"%(i+1)]
               EField_x.set_auto_mask(False)
               EField_x = EField_x [:]
            if point_data_names[i] == "EField_y":
               EField_y = data.variables["vals_nod_var%s"%(i+1)]
               EField_y.set_auto_mask(False)
               EField_y = EField_y [:]
            if point_data_names[i] == "charge_density":
               charge_density = data.variables["vals_nod_var%s"%(i+1)]
               charge_density.set_auto_mask(False)
               charge_density = charge_density[:]
            if point_data_names[i] == "voltage":
               voltage = data.variables["vals_nod_var%s"%(i+1)]
               voltage.set_auto_mask(False)
               voltage = voltage [:]


        EField_x = EField_x[1] * 1e-6  # From V/m to MV/m
        EField_y = EField_y[1] * 1e-6  # From V/m to MV/m
        EField   = np.sqrt(EField_x**2 + EField_y**2)
        voltage  = voltage[1]  * 1e-6 # from V to MV

        # ax  = plt.subplot(gs[j,k])
        # if j ==0:
        #     LV  = 0
        #     HV  = 5
        #     LevelsV = np.linspace(LV, HV, 100)
        #     labelsV = np.arange(LV,HV+1,1)
        #     locV    =  labelsV
        #
        # CS  = plt.tricontourf(X,Y,voltage, levels= LevelsV, cmap="jet")
        # # divider = make_axes_locatable(ax)
        # # cax = divider.append_axes("right", size="5%", pad=0.05)
        # cb  = plt.colorbar(CS)
        # cb.ax.tick_params(labelsize=fs)
        # cb.set_ticks(locV)
        # # ax.clabel(CS, LevelsV[::20], inline=True, fontsize=10)
        # plt.rcParams["mathtext.fontset"] = "cm"
        # plt.rcParams["text.usetex"] =True
        # if j == 0:
        #     plt.title("$\\textnormal{dV/dX (%s)~[MV/m]}$"%fluid, fontsize=fs)
        # if j < len(grads)-1:
        #     ax.set_xticks([])
        # else:
        #     ax.set_xticks([0,1])
        #     ax.set_xlabel('$X$', fontsize=fs)
        #
        # if k == 0:
        #     ax.set_yticks([0,1])
        #     ax.set_ylabel('$Y$', fontsize=fs)
        # else:
        #     ax.set_yticks([])
        #     ax.set_ylabel('', fontsize=fs)
        # ax.tick_params(axis='both', which='major', labelsize=fs)
        # ax.tick_params(axis='both', which='minor', labelsize=fs)
        #
        ax  = plt.subplot(gs[j,k])
        if k == 0:
            HE = roundup(EField.max())
            LE = 0
        if k == 0 and j==2:
            HE = 90

        if fluid == 'air':
            ERep = EField.max()
            E_fluid.append('{:04.2f}'.format(ERep))
        else:
            ERep = EField.min()
            E_fluid.append('{:04.2f}'.format(ERep))

        LevelsE = np.linspace(LE, HE, 100)
        labelsE = np.linspace(LE,HE,5)
        locE    =  labelsE
        CS  = plt.tricontourf(X,Y,EField, levels=LevelsE, cmap="jet")
        if k >3:
            cb  = plt.colorbar(CS)
            cb.set_ticks(locE)
            cb.set_ticklabels(labelsE)
            cb.ax.tick_params(labelsize=fs)
        plt.rcParams["mathtext.fontset"] = "cm"
        plt.rcParams["text.usetex"] =True

        plt.title("%s, %3.1f MV/m"%(fluid, ERep), fontsize=fs)

        if j < len(grads)-1:
            ax.set_xticks([])
        else:
            ax.set_xticks([0,1])
            ax.set_xlabel('$X$', fontsize=fs)

        if k != 0 or k+1 == 1:
            ax.set_yticks([])
            ax.set_ylabel('', fontsize=fs)
        else:
           ax.set_yticks([0,1])
           ax.set_ylabel('$Y$', fontsize=fs)

        ax.tick_params(axis='both', which='major', labelsize=fs)
        ax.tick_params(axis='both', which='minor', labelsize=fs)
        # plt.xlim(0.25,0.75)
        # plt.ylim(0.25,0.75)


    Data["%s"%fluid]  = E_fluid

EField_Data_Fluids =pd.DataFrame(dict([ (k,pd.Series(v)) for k,v in Data.items() ]))
EField_Data_Fluids.to_csv("EField_Data_Fluids.csv", encoding='utf-8', index=False)


fig.subplots_adjust(left=0.1, bottom=bottomspace)
plt.show()
