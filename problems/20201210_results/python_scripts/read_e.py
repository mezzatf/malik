from netCDF4 import Dataset as ns
import numpy as np
data  = ns('./laplace_air_square_out.e')
names = data.variables["name_nod_var"]
names.set_auto_mask(False)
point_data_names = [b"".join(c).decode("UTF-8") for c in names[:]]
print(point_data_names)

for i in np.arange(len(point_data_names)):
    if point_data_names[i] == "EField_x":
       EField_x = data.variables["vals_nod_var%s"%(i+1)]
       EField_x.set_auto_mask(False)
       EField_x = EField_x [:]
    if point_data_names[i] == "EField_y":
       EField_y = data.variables["vals_nod_var%s"%(i+1)]
       EField_y.set_auto_mask(False)
       EField_y = EField_y [:]
    if point_data_names[i] == "charge_density":
       charge_density = data.variables["vals_nod_var%s"%(i+1)]
       charge_density.set_auto_mask(False)
       charge_density = charge_density[:]
    if point_data_names[i] == "voltage":
       voltage = data.variables["vals_nod_var%s"%(i+1)]
       voltage.set_auto_mask(False)
       voltage = voltage [:]

X = data.variables["coordx"]
X.set_auto_mask(False)
X = X [:]
Y = data.variables["coordy"]
Y.set_auto_mask(False)
Y = Y [:]
import matplotlib.gridspec as gridspec

def plot_variable(X=X, Y=Y, name=EField_x):
    gs  = gridspec.GridSpec(1,1)
    fig = plt.figure(figsize=(12,8))
    ax  = plt.subplot(gs[0,0])
    L   = name.min()
    H   = name.max()
    Levels = np.linspace(L, H, 200)
    for i in np.arange(len(name)):
        CS = plt.tricontourf(X,Y,name[i], levels=Levels, cmap="hsv",shading='auto')
        if (i == 0):
             cb = plt.colorbar(CS)
        plt.pause(0.005)


plot_variable(name=EField_x)





# names = data.variables["name_elem_var"]
# names.set_auto_mask(False)
# point_data_names = [b"".join(c).decode("UTF-8") for c in names[:]]
# print(point_data_names)
#
# value2 = data.variables["vals_nod_var1"]
# value2.set_auto_mask(False)
# print(value2[:])
