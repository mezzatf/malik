from netCDF4 import Dataset as ns
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
gs  = gridspec.GridSpec(2,3)
fig = plt.figure(figsize=(12,8))
fs = 18







data  = ns('./laplace_air_square_out.e')

# GET THE X-Coord of nodes
X = data.variables["coordx"]
X.set_auto_mask(False)
X = X [:]
# GET THE Y-Coord of nodes
Y = data.variables["coordy"]
Y.set_auto_mask(False)
Y = Y [:]

# GET THE NAMES OF THE NODE VARIABLES
variable_node_names = data.variables["name_nod_var"]
variable_node_names.set_auto_mask(False)
point_data_names = [b"".join(c).decode("UTF-8") for c in variable_node_names[:]]
print(point_data_names)

# GET THE NAMES OF THE ELEMENT VARIABLES
variable_element_names = data.variables["name_elem_var"]
variable_element_names.set_auto_mask(False)
element_data_names = [b"".join(c).decode("UTF-8") for c in variable_element_names[:]]
print(element_data_names)


for i in np.arange(len(point_data_names)):
    if point_data_names[i] == "EField_x":
       EField_x = data.variables["vals_nod_var%s"%(i+1)]
       EField_x.set_auto_mask(False)
       EField_x = EField_x [:]
    if point_data_names[i] == "EField_y":
       EField_y = data.variables["vals_nod_var%s"%(i+1)]
       EField_y.set_auto_mask(False)
       EField_y = EField_y [:]
    if point_data_names[i] == "charge_density":
       charge_density = data.variables["vals_nod_var%s"%(i+1)]
       charge_density.set_auto_mask(False)
       charge_density = charge_density[:]
    if point_data_names[i] == "voltage":
       voltage = data.variables["vals_nod_var%s"%(i+1)]
       voltage.set_auto_mask(False)
       voltage = voltage [:]

for i in np.arange(len(element_data_names)):
    if element_data_names[i] == "electric_permittivity":
       electric_permittivity_rock = data.variables["vals_elem_var%seb1"%(i+1)]
       electric_permittivity_rock.set_auto_mask(False)
       electric_permittivity_rock = electric_permittivity_rock [:]
       electric_permittivity_pore = data.variables["vals_elem_var%seb2"%(i+1)]
       electric_permittivity_pore.set_auto_mask(False)
       electric_permittivity_pore = electric_permittivity_pore [:]




# electric_permittivity_pore  = electric_permittivity_pore[0]

# ax  = plt.subplot(gs[0,0])
# L   = electric_permittivity_pore.min()
# H   = electric_permittivity_pore.max()
# LevelsEP = np.linspace(L, H, 200)
# CS  = plt.tricontourf(X,Y,electric_permittivity_pore, levels=LevelsEP, cmap="jet")
# cb  = plt.colorbar(CS)


EField_x = EField_x[5] * 1e-3 # From V/m to kV/cm
voltage  = voltage[5]  * 1e-3 # from V to kV

ax1  = plt.subplot(gs[0,0])
LV  = round(voltage.min(),-2)
HV  = round(voltage.max(),-2)
LevelsV = np.linspace(LV, HV, 200)
CS  = plt.tricontourf(X,Y,voltage, levels=LevelsV, cmap="jet")
plt.rcParams["mathtext.fontset"] = "cm"
plt.rcParams["text.usetex"] =True
ax1.set_xticklabels([])
ax1.set_yticks([0,2,4,6,8,10])
ax1.tick_params(axis='both', which='major', labelsize=fs)
ax1.tick_params(axis='both', which='minor', labelsize=fs)


ax2   = plt.subplot(gs[0,1])
LE   = round(EField_x.min(),-1)
HE   = round(EField_x.max(),-1)+10
LevelsE = np.linspace(LE, HE, 200)
CS = plt.tricontourf(X,Y,EField_x, levels=LevelsE, cmap="jet")
plt.rcParams["mathtext.fontset"] = "cm"
plt.rcParams["text.usetex"] =True
ax2.set_xticklabels([])
ax2.set_yticklabels([])
ax2.tick_params(axis='both', which='major', labelsize=fs)
ax2.tick_params(axis='both', which='minor', labelsize=fs)


data  = ns('./laplace_air_circle_out.e')

# GET THE X-Coord of nodes
X = data.variables["coordx"]
X.set_auto_mask(False)
X = X [:]
# GET THE Y-Coord of nodes
Y = data.variables["coordy"]
Y.set_auto_mask(False)
Y = Y [:]

# GET THE NAMES OF THE NODE VARIABLES
variable_node_names = data.variables["name_nod_var"]
variable_node_names.set_auto_mask(False)
point_data_names = [b"".join(c).decode("UTF-8") for c in variable_node_names[:]]
print(point_data_names)

# GET THE NAMES OF THE ELEMENT VARIABLES
variable_element_names = data.variables["name_elem_var"]
variable_element_names.set_auto_mask(False)
element_data_names = [b"".join(c).decode("UTF-8") for c in variable_element_names[:]]
print(element_data_names)


for i in np.arange(len(point_data_names)):
    if point_data_names[i] == "EField_x":
       EField_x = data.variables["vals_nod_var%s"%(i+1)]
       EField_x.set_auto_mask(False)
       EField_x = EField_x [:]
    if point_data_names[i] == "EField_y":
       EField_y = data.variables["vals_nod_var%s"%(i+1)]
       EField_y.set_auto_mask(False)
       EField_y = EField_y [:]
    if point_data_names[i] == "charge_density":
       charge_density = data.variables["vals_nod_var%s"%(i+1)]
       charge_density.set_auto_mask(False)
       charge_density = charge_density[:]
    if point_data_names[i] == "voltage":
       voltage = data.variables["vals_nod_var%s"%(i+1)]
       voltage.set_auto_mask(False)
       voltage = voltage [:]




EField_x = EField_x[5] * 1e-3 # From V/m to kV/cm
voltage  = voltage[5]  * 1e-3 # from V to kV

ax3  = plt.subplot(gs[1,0])
CS  = plt.tricontourf(X,Y,voltage, levels=LevelsV, cmap="jet")
cb = plt.colorbar( orientation='horizontal')
labels = np.arange(LV,HV+50,50)
loc    =  labels
cb.set_ticks(loc)
cb.set_ticklabels(labels)
plt.rcParams["mathtext.fontset"] = "cm"
plt.rcParams["text.usetex"] =True
ax3.set_xticks([0,2,4,6,8,10])
ax3.set_yticks([0,2,4,6,8,10])
ax3.tick_params(axis='both', which='major', labelsize=fs)
ax3.tick_params(axis='both', which='minor', labelsize=fs)

ax4  = plt.subplot(gs[1,1])
CS = plt.tricontourf(X,Y,EField_x, levels=LevelsE, cmap="jet")
cb = plt.colorbar( orientation='horizontal')
labels = np.arange(LE,HE,10)
loc    =  labels
cb.set_ticks(loc)
cb.set_ticklabels(labels)
plt.rcParams["mathtext.fontset"] = "cm"
plt.rcParams["text.usetex"] =True
ax4.set_yticklabels([])
ax4.set_xticks([0,2,4,6,8,10])
ax4.tick_params(axis='both', which='major', labelsize=fs)
ax4.tick_params(axis='both', which='minor', labelsize=fs)

plt.show()



#
#
# Levels = np.linspace(L, H, 200)
# for i in np.arange(len(name)):
#     CS = plt.tricontourf(X,Y,name[i], levels=Levels, cmap="jet")
#     if (i == 0):
#          cb = plt.colorbar(CS)
#     plt.pause(0.005)


#
# EField = np.
#
# import matplotlib.gridspec as gridspec
#
# def plot_variable(X=X, Y=Y, name=EField_x):
#     gs  = gridspec.GridSpec(1,1)
#     fig = plt.figure(figsize=(12,8))
#     ax  = plt.subplot(gs[0,0])
#     L   = name.min()
#     H   = name.max()
#     Levels = np.linspace(L, H, 200)
#     for i in np.arange(len(name)):
#         CS = plt.tricontourf(X,Y,name[i], levels=Levels, cmap="jet")
#         if (i == 0):
#              cb = plt.colorbar(CS)
#         plt.pause(0.005)
#
#
# plot_variable(name=EField_x)
#
#
#
#
#
# # names = data.variables["name_elem_var"]
# # names.set_auto_mask(False)
# # point_data_names = [b"".join(c).decode("UTF-8") for c in names[:]]
# # print(point_data_names)
# #
# # value2 = data.variables["vals_nod_var1"]
# # value2.set_auto_mask(False)
# # print(value2[:])
