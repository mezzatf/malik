import numpy as np
from matplotlib import pyplot as plt
from math import pi

u=5          #x-position of the center
v=5          #y-position of the center
b=36*1e-3    #radius on the x-axis
a=18*1e-3    #radius on the y-axis
points = []
link   = "//+BSpline(5)={"
for i, t in enumerate(np.linspace(0, 2*pi, 200)):
    print ("Point (%s) = {%5.5f,%5.5f,0,dx0};"%(5+i, u+a*np.cos(t), v+b*np.sin(t)))
    link+= "%s,"%(5+i)
link += "};"
print(link)


# plt.plot(  )
# plt.grid(color='lightgray',linestyle='--')
# plt.show()
