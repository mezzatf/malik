import numpy as np
from matplotlib import pyplot as plt
from math import pi

w  = 10 #mm
h  = 10 #mm
vf = 0.1
vf = 0.01
nu = 500
vf = vf/nu

Rock_A = 10*10 #mm^2

Pore_A = vf * Rock_A
R  = np.sqrt(Pore_A/np.pi)
L  = np.sqrt(Pore_A)
b  = np.sqrt(Pore_A/(2*np.pi))
a  = 2*b

print ('%18s' %"Radius = " + " %4.3f mm"%R)
print ('%18s' %"Side = "   + " %4.3f mm"%L)
print ('%18s' %"Major radius = " + " %4.3f mm"%(a))
print ('%18s' %"Minor radius = " + " %4.3f mm"%(b))


# plt.plot(  )
# plt.grid(color='lightgray',linestyle='--')
# plt.show()
