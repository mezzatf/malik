from netCDF4 import Dataset as ns
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from mpl_toolkits.axes_grid1 import make_axes_locatable



shapes = ['ellipse_x', 'circle', 'square', 'ellipse_y']
# shapes = ['ellipse_x', 'circle']
# shapes = ['square', 'ellipse_y']
EmaxAir = []
EminWat = []
gs  = gridspec.GridSpec(len(shapes),4,wspace=0.2)

if len(shapes) == 2:
   fig = plt.figure(figsize=(16,6))
if len(shapes) == 4:
   fig = plt.figure(figsize=(16,8))
fs = 18
for n, shape in enumerate(shapes):
    j = n
    fluids = ['air', 'water']
    for f, fluid in enumerate(fluids):
        if f==0:
            k = 0
        else:
            k = f+1
        file  = 'laplace_%s_%s_out.e'%(fluid, shape)
        data  = ns(file)
        # GET THE X-Coord of nodes
        X = data.variables["coordx"]
        X.set_auto_mask(False)
        X = X [:]
        # GET THE Y-Coord of nodes
        Y = data.variables["coordy"]
        Y.set_auto_mask(False)
        Y = Y [:]
        print(j,k)
        print(j,k+1)


        # GET THE NAMES OF THE NODE VARIABLES
        variable_node_names = data.variables["name_nod_var"]
        variable_node_names.set_auto_mask(False)
        point_data_names = [b"".join(c).decode("UTF-8") for c in variable_node_names[:]]
        print(point_data_names)

        for i in np.arange(len(point_data_names)):
            if point_data_names[i] == "EField_x":
               EField_x = data.variables["vals_nod_var%s"%(i+1)]
               EField_x.set_auto_mask(False)
               EField_x = EField_x [:]
            if point_data_names[i] == "EField_y":
               EField_y = data.variables["vals_nod_var%s"%(i+1)]
               EField_y.set_auto_mask(False)
               EField_y = EField_y [:]
            if point_data_names[i] == "charge_density":
               charge_density = data.variables["vals_nod_var%s"%(i+1)]
               charge_density.set_auto_mask(False)
               charge_density = charge_density[:]
            if point_data_names[i] == "voltage":
               voltage = data.variables["vals_nod_var%s"%(i+1)]
               voltage.set_auto_mask(False)
               voltage = voltage [:]


        EField_x = EField_x[5] * 1e-3  # From V/mm to MV/m
        EField_y = EField_y[5] * 1e-3  # From V/mm to MV/m
        EField   = np.sqrt(EField_x**2 + EField_y**2)
        voltage  = voltage[5]  * 1e-3 # from V to kV

        ax  = plt.subplot(gs[j,k])
        if j ==0:
            LV  = round(voltage.min(),-2)
            HV  = round(voltage.max(),-2)
            LevelsV = np.linspace(LV, HV, 200)
            labelsV = np.arange(LV,HV+100,100)
            locV    =  labelsV

        CS  = plt.tricontourf(X,Y,voltage, levels=LevelsV, cmap="jet")
        # divider = make_axes_locatable(ax)
        # cax = divider.append_axes("right", size="5%", pad=0.05)
        cb  = plt.colorbar(CS)
        cb.ax.tick_params(labelsize=fs)
        cb.set_ticks(locV)
        # ax.clabel(CS, LevelsV[::20], inline=True, fontsize=10)
        plt.rcParams["mathtext.fontset"] = "cm"
        plt.rcParams["text.usetex"] =True
        if j == 0:
            plt.title("$\\textnormal{V (%s)~kV}$"%fluid, fontsize=fs)
        if j < len(shapes)-1:
            ax.set_xticks([])
        else:
            ax.set_xticks([0,2,4,6,8,10])
            ax.set_xlabel('$X\\mathrm{[mm]}$', fontsize=fs)

        if k == 0:
            ax.set_yticks([0,2,4,6,8,10])
            ax.set_ylabel('$Y\\mathrm{[mm]}$', fontsize=fs)
        else:
            ax.set_yticks([])
            ax.set_ylabel('', fontsize=fs)
        ax.tick_params(axis='both', which='major', labelsize=fs)
        ax.tick_params(axis='both', which='minor', labelsize=fs)


        ax  = plt.subplot(gs[j,k+1])
        if  k ==0:
            LE = 0
            HE = 80
        else:
            LE = 0
            HE = 80

        if fluid == 'air':
           if j !=2:
             EmaxAir.append(EField.max())
           else:
             index = np.argsort(EField)[-100]
             EmaxAir.append(EField[index])
             EE = EField
        if fluid == 'water':
           EminWat.append(EField.min())

        LevelsE = np.linspace(LE, HE, 200)
        labelsE = np.arange(LE,HE+20,20)
        locE    =  labelsE
        CS  = plt.tricontourf(X,Y,EField, levels=LevelsE, cmap="jet")
        cb  = plt.colorbar(CS)
        cb.set_ticks(locE)
        cb.set_ticklabels(labelsE)
        cb.ax.tick_params(labelsize=fs)
        plt.rcParams["mathtext.fontset"] = "cm"
        plt.rcParams["text.usetex"] =True
        if j == 0:
            plt.title("$\\mathrm{E~(%s)~[MV/m]}$"%fluid, fontsize=fs)

        if j < len(shapes)-1:
            ax.set_xticks([])
        else:
            ax.set_xticks([0,2,4,6,8,10])
            ax.set_xlabel('$X\\mathrm{[mm]}$', fontsize=fs)

        if k != 0 or k+1 == 1:
            ax.set_yticks([])
            ax.set_ylabel('', fontsize=fs)
        else:
           ax.set_yticks([0,2,4,6,8,10])
           ax.set_ylabel('$Y\\mathrm{[mm]}$', fontsize=fs)

        ax.tick_params(axis='both', which='major', labelsize=fs)
        ax.tick_params(axis='both', which='minor', labelsize=fs)


fig.subplots_adjust(left=0.1)
plt.show()
