from netCDF4 import Dataset as ns
import numpy as np
import matplotlib.gridspec as gridspec
from mpl_toolkits.axes_grid1.inset_locator import zoomed_inset_axes
from mpl_toolkits.axes_grid1.inset_locator import mark_inset

from matplotlib.tri import (
    Triangulation, UniformTriRefiner, CubicTriInterpolator)

data  = ns('./laplace_air_circle_top_out.e')
Es = data.variables["name_nod_var"]
Es.set_auto_mask(False)
point_data_Es = [b"".join(c).decode("UTF-8") for c in Es[:]]
print(point_data_Es)

for i in np.arange(len(point_data_Es)):
    if point_data_Es[i] == "EField_x":
       EField_x = data.variables["vals_nod_var%s"%(i+1)]
       EField_x.set_auto_mask(False)
       EField_x = EField_x [:]
    if point_data_Es[i] == "EField_y":
       EField_y = data.variables["vals_nod_var%s"%(i+1)]
       EField_y.set_auto_mask(False)
       EField_y = EField_y [:]
    if point_data_Es[i] == "voltage":
       voltage = data.variables["vals_nod_var%s"%(i+1)]
       voltage.set_auto_mask(False)
       voltage = voltage [:]

EField_x = EField_x[1] * 1e-6  # From V/m to MV/m
EField_y = EField_y[1] * 1e-6  # From V/m to MV/m
EField   = np.sqrt(EField_x**2 + EField_y**2)
voltage  = voltage[1]  * 1e-6 # from V to MV


X = data.variables["coordx"]
X.set_auto_mask(False)
X = X [:]
Y = data.variables["coordy"]
Y.set_auto_mask(False)
Y = Y [:]

def plot_variable(X=X, Y=Y, Ex= EField_x, Ey=EField_y, E_norm=EField, V=voltage):
    gs  = gridspec.GridSpec(1, 6)
    fig = plt.figure(figsize=(9,1.5))

## Ellipse
    u=0.5          #x-position of the center
    v=0.9          #y-position of the center
    a=0.020        #radius on the x-axis
    b=0.050        #radius on the y-axis

    t = np.linspace(0, 2*np.pi, 200)
    x = u+a*np.cos(t)
    y = v+b*np.sin(t)

## ELECTRIC FIELD
    triang = Triangulation(X, Y)

    ax  = plt.subplot(gs[0, 0:5])
    # ax.set_aspect(0.3)
    L   = E_norm.min()
    H   = E_norm.max()
    print(H)
    LevelsE = np.linspace(0, 1, 200)
    LabelsE = np.arange(0, 1.5, 0.5)
    LocE    = LabelsE
    # ax.quiver(triang.x[::20], triang.y[::20], Ex[::20]/E_norm[::20], Ey[::20]/E_norm[::20],
    #       units='xy', scale=30., zorder=3, color='red',
    #        width=0.003, headwidth=3., headlength=5.)
    ax.tricontour(X,Y,E_norm/H, levels=LevelsE[:30], linewidths=0.5, colors='k')
    plt.plot(x,y)
    plt.ylim(0.8,1)
    # ax.set_yticks([0.8, 0.9, 1.0])
    # ax.set_xticks([0.0, 0.5, 1.0])
    ax.set_xticks([])
    ax.set_yticks([])
    # inset_ax = zoomed_inset_axes(ax, zoom=2.5, loc=4)
    # inset_ax.axis([1.4, 1.1, 1.4, 1.1])


    ax1  = plt.subplot(gs[0, 5])
    # ax1.set_aspect(aspect=1)
    # ax1.set_aspect('equal')
    ax1.tricontour(X,Y,E_norm/H, levels=LevelsE[:30], linewidths=0.5, colors='k')
    ax1.tick_params(color='red', labelcolor='red')
    for spine in ax1.spines.values():
        spine.set_edgecolor('red')
    ax1.set_xticks([])
    ax1.set_yticks([])
    # ax1.arrow(0.490, 0.90, 0.020, 0.0, head_width=0.005, head_length=0.005, fc='k', ec='k')
    # ax1.set_xlabel("x-axis", fontsize=fs)
    # ax1.set_ylabel("y-axis", fontsize=fs)
    #plt.axhline(y= 0.9 ,color='black', lw=0.5)
    #plt.axvline(x= 0.5 ,color='black', lw=0.5)


    plt.plot(x,y)

    plt.ylim(0.840,0.960)
    plt.xlim(0.460,0.540)
    mark_inset(ax, ax1, loc1=2, loc2=3, fc="none",  lw=1, ec='r')

    plt.subplots_adjust(wspace=0.3, hspace=0)


    # CS  = plt.tricontourf(X,Y,E_norm/H, levels=LevelsE, cmap="jet" )
    # cb  = plt.colorbar(CS)
    # cb.set_ticks(LocE)
    # cb.set_ticklabels(LabelsE)
    # cb.ax.tick_params(labelsize=fs)
    # plt.ylim(0,1)

    # axins = zoomed_inset_axes(ax, 2, loc=1) # zoom = 6
    # axins.tricontourf(X,Y,E_norm/H, levels=LevelsE, cmap="jet" )
    # axins.tricontour(X,Y,E_norm/H, levels=LevelsE[:20], linewidths=0.5, colors='w')
    #
    # axins.set_xlim(0.45, 0.55)
    # axins.set_ylim(0.85, 0.95)
    # plt.xticks(visible=False)
    # plt.yticks(visible=False)
    # mark_inset(ax, axins, loc1=2, loc2=4, fc="none", ec="0.5")
    # plt.draw()



## VOLTAGE
    # ax1 = plt.subplot(gs[0, 1])
    # LV  = V.min()
    # HV  = V.max()
    # LevelsV =  np.linspace(0, HV/HV, 100)
    # LabelsV =  np.arange(0, 1.5, 0.5)
    # LocV    =  LabelsV
    # ax1.tricontour(X,Y,V/HV, levels= LevelsV[20:80], linewidths= 0.5, colors='k')
    # CS1  = plt.tricontourf(X,Y,V/HV, levels=LevelsV, cmap="jet" )
    # ax1.quiver(triang.x[::20], triang.y[::20], Ex[::20]/E_norm[::20], Ey[::20]/E_norm[::20],
    #       units='xy', scale=20., zorder=3, color='red',
    #       width=0.005, headwidth=3., headlength=5.)
    #
    # cb1  = plt.colorbar(CS1)
    # cb1.set_ticks(LocV)
    # cb1.set_ticklabels(LabelsV)
    # cb1.ax.tick_params(labelsize=fs)
    # plt.ylim(0,1)
    plt.show()



plot_variable(Ex= EField_x, Ey=EField_y, E_norm=EField, V=voltage)





# Es = data.variables["E_elem_var"]
# Es.set_auto_mask(False)
# point_data_Es = [b"".join(c).decode("UTF-8") for c in Es[:]]
# print(point_data_Es)
#
# value2 = data.variables["vals_nod_var1"]
# value2.set_auto_mask(False)
# print(value2[:])
