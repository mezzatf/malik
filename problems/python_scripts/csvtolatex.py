#input file

from os import walk
import os

file = "EField_Data_Fluids.csv"
fin  = open(file, "rt")
fout = open(file.replace(".csv", "TexTable.csv"), "wt")
for n, line in enumerate(fin):
    if n ==0:
        commas = line.count(',')+1
        fout.write("\\"+"begin{tabular}{"+"c|"*commas+"}\n")
        fout.write("\hline " + "\hline\n")
        fout.write(line.replace(",", "&")+"\\"+"\\"+"\n")
        fout.write("\hline " + "\hline" +"\n")
    else:
	    fout.write(line.replace(",", "&")+"\\"+"\\" +"\n")
fin.close()
fout.close()




# fin = open("lapalce_brine.txt", "rt")
# #output file to write the result to
# fout = open("out.txt", "wt")
# #for each line in the input file
# for line in fin:
# 	#read replace the string and write to output file
# 	fout.write(line.replace('pyton', 'python'))
# #close input and output files
# fin.close()
# fout.close()
