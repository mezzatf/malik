from netCDF4 import Dataset as ns
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from mpl_toolkits.axes_grid1 import make_axes_locatable
import pandas as pd
import math
import os

EField_Data_Fluids = os.path.expanduser('./EField_Data_Fluids.csv')
Data = pd.read_csv(EField_Data_Fluids, delimiter=",")

plt.rcParams["mathtext.fontset"] = "cm"
plt.rcParams["text.usetex"] =True

lws = 2
fs  = 20
ms  = 10

gs   = gridspec.GridSpec(1,2,wspace=0.2)

fig  = plt.figure(figsize=(10,3.5))

ax   = plt.subplot(gs[0,0])
plt.plot(Data['grad'], Data['air'], "-*", lw=lws, markersize=ms,  label='air, $\\varepsilon=1$')
plt.xlim(0,35)
plt.ylim(0,60)
plt.legend(loc=0,  fontsize="x-large")
ax.tick_params(axis='both', which='major', labelsize=fs)
ax.tick_params(axis='both', which='minor', labelsize=fs)
plt.xlabel("$E_{BC}~\\mathrm{[MV/m]}$", fontsize=fs)
plt.ylabel("$E_{Calc}~\\mathrm{[MV/m]}$", fontsize=fs)
plt.grid(color='grey', which="both",ls="--")


ax   = plt.subplot(gs[0,1])
# plt.plot(Data['grad'], Data['brine20'],  "-s", label='brine20')
plt.plot(Data['grad'], Data['brine10'],  "-<", lw=lws, markersize=ms,  label='10\% brine, $\\varepsilon=54$')
plt.plot(Data['grad'], Data['brine02'],  "->", lw=lws, markersize=ms,  label='2\% brine, $\\varepsilon=63$')
plt.plot(Data['grad'], Data['water']  ,  "-D", lw=lws, markersize=ms,  label='water, $\\varepsilon=80$')
plt.xlim(0,35)
plt.ylim(0,10)

plt.xlabel("$E_{BC}~\\mathrm{[MV/m]}$", fontsize=fs)

plt.legend(loc=0, fontsize="x-large")
ax.tick_params(axis='both', which='major', labelsize=fs)
ax.tick_params(axis='both', which='minor', labelsize=fs)
plt.grid(color='grey', which="both",ls="--")

fig.subplots_adjust(bottom=0.2)
plt.show()
