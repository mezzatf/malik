ls = 5;

w   = 10; // mm
h   = 10;  // mm
R   = 0.5;   // mm
dx0 = 0.0001;
dx1 = 0.20;
vf  = 0.001;

r   = 0.025;

Point(1) = {0, 0, 0, dx1};
Point(2) = {w, 0, 0, dx1};
Point(3) = {w, h, 0, dx1};
Point(4) = {0, h, 0, dx1};
//+
Line(1) = {1, 2};
//+
Line(2) = {2, 3};
//+
Line(3) = {3, 4};
//+
Line(4) = {4, 1};
//+
Point(5) = {w/2,h/2,0,dx0};
Point(6) = {w/2,h/2-r,0,dx0};
Point(7) = {w/2,h/2+r,0,dx0};
//+
Circle(5) = {7, 5, 6};
//+
Circle(6) = {6, 5, 7};
//+
Curve Loop(1) = {4, 1, 2, 3};
//+
Curve Loop(2) = {5, 6};
//+
Plane Surface(1) = {1, 2};
//+
Plane Surface(2) = {2};
//+
Physical Curve("wall") = {3, 1};
//+
Physical Curve("HV") = {4};
//+
Physical Curve("GE") = {2};
//+
Physical Surface("rock") = {1};
//+
Physical Surface("pore") = {2};
