from netCDF4 import Dataset as ns
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from mpl_toolkits.axes_grid1.inset_locator import zoomed_inset_axes
from mpl_toolkits.axes_grid1.inset_locator import mark_inset

from matplotlib.tri import (
    Triangulation, UniformTriRefiner, CubicTriInterpolator)

data  = ns('./laplace_air_ellipse_y_top_out.e')
Es = data.variables["name_nod_var"]
Es.set_auto_mask(False)
point_data_Es = [b"".join(c).decode("UTF-8") for c in Es[:]]
print(point_data_Es)

for i in np.arange(len(point_data_Es)):
    if point_data_Es[i] == "EField_x":
       EField_x = data.variables["vals_nod_var%s"%(i+1)]
       EField_x.set_auto_mask(False)
       EField_x = EField_x [:]
    if point_data_Es[i] == "EField_y":
       EField_y = data.variables["vals_nod_var%s"%(i+1)]
       EField_y.set_auto_mask(False)
       EField_y = EField_y [:]
    if point_data_Es[i] == "voltage":
       voltage = data.variables["vals_nod_var%s"%(i+1)]
       voltage.set_auto_mask(False)
       voltage = voltage [:]

EField_x = EField_x[1] * 1e-6  # From V/m to MV/m
EField_y = EField_y[1] * 1e-6  # From V/m to MV/m
EField   = np.sqrt(EField_x**2 + EField_y**2)
voltage  = voltage[1]  * 1e-6 # from V to MV


X = data.variables["coordx"]
X.set_auto_mask(False)
X = X [:]
Y = data.variables["coordy"]
Y.set_auto_mask(False)
Y = Y [:]
fs = 20
def plot_variable(X=X, Y=Y, Ex= EField_x, Ey=EField_y, E_norm=EField, V=voltage):
    gs  = gridspec.GridSpec(1, 6)
    fig = plt.figure(figsize=(9,2.5))

## ELECTRIC FIELD
    triang = Triangulation(X, Y)

    ax  = plt.subplot(gs[0, 0:5])
    ax.set_aspect(1.0)
    L   = E_norm.min()
    H1   = 4
    # print(H)
    LevelsE = np.linspace(0, 10, 200)
    LabelsE = np.arange(0, 12, 2)
    LocE    = LabelsE
    # ax.tricontour(X,Y,E_norm/H, levels=LevelsE[:20], linewidths=0.5, colors='w')

    CS  = plt.tricontourf(X,Y,E_norm/H1, levels=LevelsE, cmap="jet")
    cb  = plt.colorbar(CS, orientation = "horizontal")
    cb.set_ticks(LocE)
    cb.set_ticklabels(LabelsE)
    cb.ax.tick_params(labelsize=fs)
    plt.ylim(0.8, 1)
    plt.xlim(0, 1)



    ax1  = plt.subplot(gs[0, 5])
    ax1.set_aspect('equal')
    LevelsE1 = np.linspace(0, 3, 200)
    LabelsE1 = np.arange(0, 4, 1)
    LocE1    = LabelsE1
    CS1      = plt.tricontourf(X,Y,E_norm/4, levels=LevelsE1, cmap="jet")
    cb1      = plt.colorbar(CS1, orientation = "horizontal")
    cb1.set_ticks(LocE1)
    cb1.set_ticklabels(LabelsE1)
    cb1.ax.tick_params(labelsize=fs)

    ax1.tick_params(color='red', labelcolor='red')
    for spine in ax1.spines.values():
        spine.set_edgecolor('red')
    ax1.set_xticks([])
    ax1.set_yticks([])
    # ax1.arrow(0.490, 0.90, 0.020, 0.0, head_width=0.005, head_length=0.005, fc='k', ec='k')
    # ax1.set_xlabel("x-axis", fontsize=fs)
    # ax1.set_ylabel("y-axis", fontsize=fs)
    #plt.axhline(y= 0.9 ,color='black', lw=0.5)
    #plt.axvline(x= 0.5 ,color='black', lw=0.5)


    # plt.plot(x,y)

    plt.ylim(0.880,0.920)
    plt.xlim(0.480,0.520)
    mark_inset(ax, ax1, loc1=2, loc2=3, fc="none",  lw=1, ec='r')

    plt.subplots_adjust(wspace=0.3, hspace=0)

    # H2   = 4.0
    # LevelsE1 = np.linspace(0, 2.5, 200)
    # LabelsE1 = np.arange(0, 2+0.5, 0.5)
    # LocE1    = LabelsE1
    # axins = zoomed_inset_axes(ax, 2, loc=3) # zoom = 6
    # CS1 = axins.tricontourf(X,Y,E_norm/H2, levels=LevelsE1, cmap="jet" )
    # cb1  = plt.colorbar(CS1)
    # cb1.set_ticks(LocE1)
    # cb1.set_ticklabels(LabelsE1)
    # cb1.ax.tick_params(labelsize=fs)
    #
    # # axins.tricontour(X,Y,E_norm/H, levels=LevelsE[:20], linewidths=0.5, colors='w')
    # #
    # axins.set_xlim(0.40, 0.6)
    # axins.set_ylim(0.80, 1.0)
    # plt.xticks(visible=False)
    # plt.yticks(visible=False)
    # mark_inset(ax, axins, loc1=1, loc2=4, fc="none", ec="0.5")
    # plt.draw()

    plt.show()



plot_variable(Ex= EField_x, Ey=EField_y, E_norm=EField, V=voltage)





# Es = data.variables["E_elem_var"]
# Es.set_auto_mask(False)
# point_data_Es = [b"".join(c).decode("UTF-8") for c in Es[:]]
# print(point_data_Es)
#
# value2 = data.variables["vals_nod_var1"]
# value2.set_auto_mask(False)
# print(value2[:])
