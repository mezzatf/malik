import numpy as np
from matplotlib import pyplot as plt
import matplotlib.gridspec as gridspec

#Paschen's law

A      = 112.50  * 1e-1 * 1e6 #1/(Pa.m)

gamma  = 0.001

B      = 2737.50  * 1e-6 * 1e-1 * 1e6 #MV/(Pa.m)

Pr     = 101325.0 * 1e-6 #Pa

d      = np.arange(0.000007, 1.000001, 0.000001)

C1     = B*Pr
C2     = A*Pr/ np.log(1./gamma + 1)

V_bd_n = C1*d/np.log(C2*d)
E_bd   = V_bd_n/d
E_bd2  = abs(C1*(np.log(C2*d) - 1.0) / np.log(C2*d)**2.0)

fs     = 14 #font_size for the plot
ms     = 16 #marker_size
lws    = 3

gs  = gridspec.GridSpec(1,1)
fig = plt.figure(figsize=(6,4))
ax  = plt.subplot(gs[0,0])

ax.plot(d*Pr, V_bd_n, ":", lw=lws,  color="black", label="$V_{thr}$")
index_min = np.argmin(V_bd_n)
print(index_min)

plt.yscale('log')
plt.xscale('log')
plt.ylabel("$V_{thr}~[MV]$", fontsize=fs)
plt.xlabel("$pd~[MPa \cdot m]$", fontsize=fs)

ax1 = ax.twinx()
plt.plot(d*Pr, E_bd,  lw=lws, color="red", label="$E_{thr}$")
#plt.plot(d[:9]*Pr, E_bd2[:9], ":", lw=lws, color="blue", label="$E_{thr2, left}$")
plt.axhline(y = 25, color ="green", linestyle ="--", label="$E_{Calc}$ 15 MV/m BCs")

plt.yscale('log')
plt.xscale('log')
plt.ylabel("$E_{thr}~[MV/m]$", color="red", fontsize=fs)
plt.xlabel("$pd~[MPa \cdot m]$", fontsize=fs)
plt.rcParams['xtick.labelsize']  = fs
plt.rcParams['ytick.labelsize']  = fs
plt.rcParams["mathtext.fontset"] = "cm"
plt.rcParams["text.usetex"]      = True
plt.legend(loc=0, numpoints = 1, prop={"size":fs})
plt.tight_layout()
plt.show()
