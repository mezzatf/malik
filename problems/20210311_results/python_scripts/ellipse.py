import numpy as np
from matplotlib import pyplot as plt
from math import pi
import os
import sys

w = 1.0
h = 1.0
porosity = 0.20

r2 = porosity*w*h/np.pi
r  = np.sqrt(porosity*w*h/np.pi)
a  = r+0.25*r #radius on the x-axis
b  = r2/a     #radius on the y-axis

u=0.5          #x-position of the center
v=0.5          #y-position of the center

dx0 = a/10
dx1 = 0.05

start  = "w   = 1;      // m\n"
start += "h   = 1;      // m\n"
start += "dx0 = %5.3f;     // m\n"%dx0
start += "dx1 = 0.05;   // m\n"

points  = "Point(1) = {0, 0, 0, dx1};\n"
points += "Point(2) = {w, 0, 0, dx1};\n"
points += "Point(3) = {w, h, 0, dx1};\n"
points += "Point(4) = {0, h, 0, dx1};\n"
points += "//+\n"
points += "Line(1) = {1, 2};\n"
points += "//+\n"
points += "Line(2) = {2, 3};\n"
points += "//+\n"
points += "Line(3) = {3, 4};\n"
points += "//+\n"
points += "Line(4) = {4, 1};\n"
points += "//+\n"


link   = "//+ \nBSpline(5)={"
for i, t in enumerate(np.linspace(0, 2*pi, 200)):
    points+= ("Point (%s) = {%5.5f,%5.5f,0,dx0};"%(5+i, u+a*np.cos(t), v+b*np.sin(t)))+"\n"
    link  += "%s,"%(5+i)
link += "5};\n\n"

curve  = "//+\n"
curve += "Curve Loop(1) = {4, 1, 2, 3};\n"
curve += "//+\n"
curve += "Curve Loop(2) = {5};\n"
curve += "//+\n"
curve += "Plane Surface(1) = {1, 2};\n"
curve += "//+\n"
curve += "Plane Surface(2) = {2};\n"
curve += "//+\n"
curve += 'Physical Curve("HV") = {4};\n'
curve += "//+\n"
curve += 'Physical Curve("GE") = {2};\n'
curve += "//+\n"
curve += 'Physical Curve("wall") = {3, 1};\n'
curve += "//+\n"
curve += 'Physical Surface("rock") = {1};\n'
curve += "//+\n"
curve += 'Physical Surface("pore") = {2};\n'

#print(start+points+link+curve)
porosity100 = str(int(porosity*100)).zfill(2)
text_file = open("./pore_ellipse_p"+porosity100+".geo", "w")
n = text_file.write(start+points+link+curve)
text_file.close()

# plt.plot(  )
# plt.grid(color='lightgray',linestyle='--')
# plt.show()
