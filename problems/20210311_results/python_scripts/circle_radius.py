import numpy as np
from matplotlib import pyplot as plt
from math import pi

w = 1.0
h = 1.0
for porosity in np.array([0.20, 0.10, 0.05, 0.01]):
    r2 = porosity*w*h/np.pi
    r  = np.sqrt(porosity*w*h/np.pi)
    a  = r+0.25*r
    b  = r2/a
    print ("r=%5.3f"%r)
    print ("r2=%5.3f"%r2)
    print ("a=%5.3f"%a)
    print ("b=%5.3f"%b)
