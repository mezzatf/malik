from netCDF4 import Dataset as ns
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from mpl_toolkits.axes_grid1.inset_locator import zoomed_inset_axes
from mpl_toolkits.axes_grid1.inset_locator import mark_inset

from matplotlib.tri import (
    Triangulation, UniformTriRefiner, CubicTriInterpolator)

def plot_variable(X, Y, Ex, Ey, E_norm, V, plot="no", VR=""):
    fs = 20
    gs  = gridspec.GridSpec(1, 1)
    fig = plt.figure(figsize=(5,5))

## ELECTRIC FIELD
    triang = Triangulation(X, Y)

    ax   = plt.subplot(gs[0,0])
    ax.set_aspect(1.0)

    # print(H)
    LevelsE = np.linspace(0, 2, 200)
    LabelsE = np.arange(0, 3, 1)
    LocE    = LabelsE

    CS  = plt.tricontourf(X,Y,E_norm, levels=LevelsE, cmap="jet")
    cb  = plt.colorbar(CS)
    cb.set_ticks(LocE)
    cb.set_ticklabels(LabelsE)
    cb.ax.tick_params(labelsize=fs)

    plt.rcParams["mathtext.fontset"] = "cm"
    plt.rcParams["text.usetex"] =True
    ax.set_xticks([0,1])
    ax.set_xlabel('$X$', fontsize=fs)
    ax.tick_params(axis='both', which='major', labelsize=fs)

    ax.set_yticks([0,1])
    ax.set_ylabel('$Y$', fontsize=fs)
    ax.tick_params(axis='both', which='minor', labelsize=fs)

    plt.title('Volumatric ratio=%s'%VR, fontsize=fs)
    plt.savefig('./figures/'+file+'.png')
    if plot == "yes":
       plt.show()



files  = []
files.append('laplace_air_circle_p01')
files.append('laplace_air_circle_p05')
files.append('laplace_air_circle_p10')
files.append('laplace_air_circle_p20')
files.append('laplace_air_ellipse_p01')
files.append('laplace_air_ellipse_p05')
files.append('laplace_air_ellipse_p10')
files.append('laplace_air_ellipse_p20')

VRs   = [1,5,10,20,1,5,10,20]

for i in np.arange(len(files)):
    file = files[i]
    VR   = VRs[i]

    data  = ns('./'+file+'_out.e')

    Es = data.variables["name_nod_var"]
    Es.set_auto_mask(False)
    point_data_Es = [b"".join(c).decode("UTF-8") for c in Es[:]]
    print(point_data_Es)

    for i in np.arange(len(point_data_Es)):
        if point_data_Es[i] == "EField_x":
           EField_x = data.variables["vals_nod_var%s"%(i+1)]
           EField_x.set_auto_mask(False)
           EField_x = EField_x [:]
        if point_data_Es[i] == "EField_y":
           EField_y = data.variables["vals_nod_var%s"%(i+1)]
           EField_y.set_auto_mask(False)
           EField_y = EField_y [:]
        if point_data_Es[i] == "voltage":
           voltage = data.variables["vals_nod_var%s"%(i+1)]
           voltage.set_auto_mask(False)
           voltage = voltage [:]

    EField_x = EField_x[1]
    EField_y = EField_y[1]
    EField   = np.sqrt(EField_x**2 + EField_y**2)
    voltage  = voltage[1]

    X = data.variables["coordx"]
    X.set_auto_mask(False)
    X = X [:]
    Y = data.variables["coordy"]
    Y.set_auto_mask(False)
    Y = Y [:]




    plot_variable(X=X, Y=Y, Ex= EField_x, Ey=EField_y, E_norm=EField, V=voltage, VR=VR)
