#input file

from os import walk
import os

old_string = "_5MV.i"
new_string = "_30MV.i"

old_value = "5e6"
new_value = "30e6"

files = os.listdir()
inputfiles = []
for file in files:
    if old_string in file:
        fin  = open(file, "rt")
        fout = open(file.replace(old_string, new_string), "wt")
        for line in fin:
            #read replace the string and write to output file
        	fout.write(line.replace(old_value, new_value))
        fin.close()
        fout.close()
