#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 21 09:15:00 2019

@author: M.Ezzat
"""


import numpy as np
import matplotlib.pyplot as plt
from scipy.constants import k, R
import os
import pandas as pd
from numpy import trapz


PowerFile = os.path.expanduser('./Lisitsyn_1998_pulse_profile.csv')
data      = pd.read_csv(PowerFile, delimiter=",")
TExp      = data['time']*1e3
VExp      = data['volt']

VMax  =   max(VExp)
VNorm =   VExp/VMax



tt = np.arange(0.0, 2.2,1e-3)*1e-6
T  = np.array([3e6, 9e6, 27e6])




fs  = 26 #font_size for the plot
ms  = 16 #marker_size
lws = 6
plt.figure(figsize=(13,7.5))

EndValues=[]
#plt.axvspan(0, 500, ymax=0.91 ,alpha=0.5, color='Grey', label="$\\tau_D =\; 500\; \mathrm{ns}$")
plt.plot(TExp[::2], VNorm[::2], "-o", markersize=ms,  lw= lws, color="black", label="$\mathrm{Experiment}$")

VV  = np.tanh(tt*9e6)
VV3 = np.sin(2.0*np.pi*tt/8000e-9)
VPulse = (VV-VV3)/0.8

plt.plot(tt * 1e9 , VV, ":", markersize=ms, lw= lws, color="orange", label="_nolabel_")
plt.plot(tt * 1e9 , VPulse, ":", markersize=ms, lw= lws, color="blue", label="_nolabel_")
#plt.plot(tt * 1e9 , VV*VV2, "-", markersize=ms, lw= lws, color="red", label="_nolabel_")

#plt.plot([tt[m]* 1e9, tt[m]* 1e9]  , [0,1], markers[i], markersize=ms, lw= lws, color=colors[i])

X = 500
plt.ylabel("$f\\left(t\\right)$", fontsize=fs)
plt.xlabel("$t\mathrm{[ns]}$", fontsize=fs)
plt.xlim(0,2000)
#plt.ylim(0,1.1)
plt.grid(color='grey', which="both",ls="--")
plt.rcParams['xtick.labelsize']=fs
plt.rcParams['ytick.labelsize']=fs
plt.rcParams["mathtext.fontset"] = "cm"
plt.rcParams["text.usetex"] =True
plt.legend(loc=7, numpoints = 1, prop={"size":fs})
# plt.xticks([0,250,500,EndValues[0],EndValues[1],EndValues[2],1000,1250,1500,1750,2000], rotation=45)
plt.show()
