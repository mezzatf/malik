#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt
from scipy.constants import k, R
import os
import pandas as pd
from numpy import trapz


PowerFile = os.path.expanduser('./Lisitsyn_1998_pulse_profile.csv')
data      = pd.read_csv(PowerFile, delimiter=",")
TExp      = data['time']
VExp      = data['volt']

VMax  =   max(VExp)
VNorm =   VExp/VMax


tt = np.arange(0.0, 2.2,1e-3)*1e-6
T  = np.array([3e6, 9e6, 27e6])

fs  = 26 #font_size for the plot
ms  = 18 #marker_size
lws = 8
plt.figure(figsize=(13,7.5))

EndValues=[]
#plt.axvspan(0, 500, ymax=0.91 ,alpha=0.5, color='Grey', label="$\\tau_D =\; 500\; \mathrm{ns}$")
plt.plot(TExp[::2], VNorm[::2], "*", markersize=ms,  lw= lws, color="blue", label="$\\textnormal{Experiment by Lisitsyn (1998)}$")

VV     = np.tanh(tt*9e6)
VV3    = np.sin(2.0*np.pi*tt/8000e-9)
VPulse = (VV-VV3)/0.8

#plt.plot(tt * 1e9 , VV, ":", markersize=ms, lw= lws, color="orange", label="_nolabel_")
plt.plot(tt * 1e6 , VPulse, ":", markersize=ms, lw= lws, color="red", label="$\\textnormal{To be used in the Simulation}$")
#plt.plot(tt * 1e9 , VV*VV2, "-", markersize=ms, lw= lws, color="red", label="_nolabel_")

#plt.plot([tt[m]* 1e9, tt[m]* 1e9]  , [0,1], markers[i], markersize=ms, lw= lws, color=colors[i])

X = 500
plt.ylabel("$f\\left(t\\right)$", fontsize=fs)
plt.xlabel("$t\mathrm{[\\mu s]}$", fontsize=fs)
plt.xlim(0,2)
#plt.ylim(0,1.1)
plt.grid(color='grey', which="both",ls="--")
plt.rcParams['xtick.labelsize']=fs
plt.rcParams['ytick.labelsize']=fs
plt.rcParams["mathtext.fontset"] = "cm"
plt.rcParams["text.usetex"] =True
plt.legend(loc=0, numpoints = 1, prop={"size":fs})
# plt.xticks([0,250,500,EndValues[0],EndValues[1],EndValues[2],1000,1250,1500,1750,2000], rotation=45)
plt.show()



#
# plt.plot(dataset[varkeys[0]], dataset[varkeys[1]])
# plt.show()
